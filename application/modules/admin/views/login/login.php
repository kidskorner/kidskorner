<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
	<meta charset="utf-8">
	<title>KidsKorner | Sign In</title>
	<!-- SEO Meta Tags-->
	<meta name="description" content="KidsKorner">
	<meta name="keywords" content="">
	<meta name="author" content="KidsKorner">
	<!-- Viewport-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicon and Touch Icons-->
	<link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('assets/img/apple-touch-icon.png') ?>">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('assets/img/favicon-32x32.png') ?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/img/favicon-16x16.png') ?>">
	<link rel="manifest" href="site.webmanifest">
	<link rel="mask-icon" color="#fe6a6a" href="safari-pinned-tab.svg">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="theme-color" content="#ffffff">
	<!-- Vendor Styles including: Font Icons, Plugins, etc.-->
	<link rel="stylesheet" media="screen" href="<?= base_url('assets/vendor/simplebar/dist/simplebar.min.css') ?>" />
	<link rel="stylesheet" media="screen" href="<?= base_url('assets/vendor/tiny-slider/dist/tiny-slider.css') ?>" />
	<!-- Main Theme Styles + Bootstrap-->
	<link rel="stylesheet" media="screen" href="<?= base_url('assets/css/theme.min.css') ?>">

</head>
<!-- Body-->

<body class="handheld-toolbar-enabled">

	<main class="page-wrapper">

		<div class="container py-4 py-lg-5 my-3">
			<div class="row justify-content-center">
				<div class="col-md-6">
					<div class="text-center">
						<img class="mb-4" src="<?= base_url('assets/img/logo.png') ?>" alt="logo" />
					</div>

					<div class="card border-0 shadow">
						<div class="card-body">
							<h3 class="h4 mb-5 text-center">Sign in</h3>
							<form class="needs-validation" id="login-form" novalidate>
								<input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>">
								<div class="input-group" id="email-validation"><i class="ci-mail position-absolute top-50 translate-middle-y text-muted fs-base ms-3"></i>
									<input class="form-control rounded-start" type="email" id="email" name="email" placeholder="Email" required>
								</div>
								<div class="input-group mt-3" id="password-validation"><i class="ci-locked position-absolute top-50 translate-middle-y text-muted fs-base ms-3"></i>
									<div class="password-toggle w-100">
										<input class="form-control" type="password" id="password" name="password" placeholder="Password" required>
										<label class="password-toggle-btn" aria-label="Show/hide password">
											<input class="password-toggle-check" type="checkbox"><span class="password-toggle-indicator"></span>
										</label>
									</div>
								</div>
								<div class="d-flex flex-wrap justify-content-between mt-3">
									<div class="form-check">
										<input class="form-check-input" type="checkbox" checked id="remember_me">
										<label class="form-check-label" for="remember_me">Remember me</label>
									</div><a class="nav-link-inline fs-sm" href="#">Forgot password?</a>
								</div>
								<hr class="mt-4">
								<div class="text-end pt-4">
									<button class="btn btn-primary" type="submit" id="login"><i class="ci-sign-in me-2 ms-n21"></i>Sign In</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<!-- Vendor scrits: js libraries and plugins-->
	<script src="<?= base_url('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') ?>"></script>
	<script src="<?= base_url('assets/vendor/simplebar/dist/simplebar.min.js') ?>"></script>
	<script src="<?= base_url('assets/vendor/tiny-slider/dist/min/tiny-slider.js') ?>"></script>
	<script src="<?= base_url('assets/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js') ?>"></script>
	<!-- Main theme script-->
	<script src="<?= base_url('assets/js/theme.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/jquery.js') ?>"></script>
	<link rel="stylesheet" href="<?= base_url('assets/css/jquery.toast.css') ?>">
	<script type="text/javascript" src="<?= base_url('assets/js/jquery.toast.js') ?>"></script>
</body>

</html>
<script>
	$(function() {
		$('#login').click(function(e) {
			e.preventDefault();
			$.ajax({
				method: "POST",
				url: "<?= base_url('admin/login/login') ?>",
				dataType: 'json',
				data: $('#login-form').serialize(),
				beforeSend: function() {
					$('#login').text('Loading...').attr('disabled', 'true');
					$('.email').remove();
					$('.password').remove();
				},
				success: function(response) {
					$('#login').text('Sign in').removeAttr('disabled');

					if (response.status.email) {
						$('#email-validation').after(response.status.email);
					}

					if (response.status.password) {
						$('#password-validation').after(response.status.password);
					}

					if (response.status == 'success') {
						window.location.href = '<?= base_url('admin/dashboard') ?>';
					}

					if (response.status == 'error') {
						$.toast({
							heading: 'Error',
							text: response.message,
							icon: response.status
						});
					}
				}
			});
		});
	});
</script>