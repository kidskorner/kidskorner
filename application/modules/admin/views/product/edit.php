<div class="py-4">
    <div class="container">
        <div class="card">
            <div class="card-header fw-bold">
                Update Product
                <a href="<?= base_url('admin/product') ?>" class="btn btn-primary btn-sm float-end">Back</a>
            </div>
            <div class="card-body">
                <form action="<?= base_url('admin/product/update') ?>" method="POST" enctype="multipart/form-data" autocomplete="off">
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                    <input type="hidden" name="eimage_1" value="<?= $product->image_1 ?>">
                    <input type="hidden" name="eimage_2" value="<?= $product->image_2 ?>">
                    <input type="hidden" name="eimage_3" value="<?= $product->image_3 ?>">
                    <input type="hidden" name="eimage_4" value="<?= $product->image_4 ?>">
                    <input type="hidden" name="eimage_5" value="<?= $product->image_5 ?>">
                    <input type="hidden" name="id" id="id" value="<?= $product->id ?>">
                    <div class="row g-2 mb-2">
                        <div class="col-md-4">
                            <div class="form-floating">
                                <select class="form-select <?= form_error('category_id') ? 'is-invalid' : ''; ?>" name="category_id" aria-label="Floating label select example">
                                    <option value="">Select Category</option>
                                    <?php foreach ($categories as $category) : ?>
                                        <option value="<?= $category->id ?>" <?= (set_value('category_id') == $category->id) || ($category->id == $product->category_id)  ? 'selected' : '' ?>><?= $category->name ?></option>
                                    <?php endforeach ?>
                                </select>
                                <label for="floatingSelect">Category</label>
                                <?= form_error('category_id', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-floating">
                                <select class="form-select <?= form_error('subcategory_id') ? 'is-invalid' : ''; ?>" name="subcategory_id" aria-label="Floating label select example">
                                    <option value="">Select Subcategory</option>
                                    <?php foreach ($subcategories as $subcategory) : ?>
                                        <option value="<?= $subcategory->id ?>" <?= (set_value('subcategory_id') == $subcategory->id) || ($subcategory->id == $product->subcategory_id)  ? 'selected' : '' ?>><?= $subcategory->name ?></option>
                                    <?php endforeach ?>
                                </select>
                                <label for="floatingSelect">Subcategory</label>
                                <?= form_error('subcategory_id', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-floating">
                                <input type="text" class="form-control <?= form_error('name') ? 'is-invalid' : ''; ?>" name="name" value="<?= (set_value('name')) ? set_value('name') : $product->name; ?>" placeholder="name">
                                <label for="floatingInputGrid">Product</label>
                                <?= form_error('name', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row g-2 mb-2">
                        <div class="col-md-4">
                            <div class="form-floating">
                                <input type="text" class="form-control <?= form_error('sku') ? 'is-invalid' : ''; ?>" name="sku" value="<?= (set_value('sku')) ? set_value('sku') : $product->sku; ?>" placeholder="sku">
                                <label for="floatingInputGrid">SKU</label>
                                <?= form_error('sku', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-floating">
                                <input type="text" class="form-control <?= form_error('regular_price') ? 'is-invalid' : ''; ?>" name="regular_price" value="<?= (set_value('regular_price')) ? set_value('regular_price') : $product->regular_price; ?>" placeholder="regular price">
                                <label for="floatingInputGrid">Regular Price</label>
                                <?= form_error('regular_price', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-floating">
                                <input type="text" class="form-control <?= form_error('sale_price') ? 'is-invalid' : ''; ?>" name="sale_price" value="<?= (set_value('sale_price')) ? set_value('sale_price') : $product->sale_price; ?>" placeholder="sale price">
                                <label for="floatingInputGrid">Sale Price</label>
                                <?= form_error('sale_price', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row g-2 mb-2">
                        <div class="col-md-6">
                            <div class="form-floating">
                                <textarea name="short_description" class="form-control <?= form_error('short_description') ? 'is-invalid' : ''; ?>" placeholder="Short description" id="floatingTextarea"><?= (set_value('short_description')) ? set_value('short_description') : $product->short_description; ?></textarea>
                                <label for="floatingTextarea">Short Description</label>
                                <?= form_error('short_description', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <textarea name="description" class="form-control <?= form_error('description') ? 'is-invalid' : ''; ?>" placeholder="Description" id="floatingTextarea"><?= (set_value('description')) ? set_value('description') : $product->description; ?></textarea>
                                <label for="floatingTextarea">Description</label>
                                <?= form_error('description', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-5">
                        <div class="col">
                            <div>
                                <label for="formFileLg" class="form-label">Image 1</label>
                                <input class="form-control " id="formFileLg" name="image_1" type="file">
                                <?= form_error('image_1', '<span class="text-danger">', '</span>'); ?>
                                <img src="<?= base_url('images/product/' . $product->image_1) ?>" class="img-thumbnail mt-2" width="100" height="50" alt="image-1">
                            </div>
                        </div>
                        <div class="col">
                            <div>
                                <label for="formFileLg" class="form-label">Image 2</label>
                                <input class="form-control " id="formFileLg" name="image_2" type="file">
                                <?= form_error('image_2', '<span class="text-danger">', '</span>'); ?>
                                <?php if ($product->image_2) : ?>
                                    <div id="img-image_2">
                                        <img src="<?= base_url('images/product/' . $product->image_2) ?>" class="img-thumbnail mt-2" width="100" height="50" alt="image-2">
                                        <button class="btn btn-danger btn-sm delete-image" data-image="<?= $product->image_2 ?>" data-column="image_2" type="button"><i class="ci-trash"></i></button>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="col">
                            <div>
                                <label for="formFileLg" class="form-label">Image 3</label>
                                <input class="form-control " id="formFileLg" name="image_3" type="file">
                                <?= form_error('image_3', '<span class="text-danger">', '</span>'); ?>
                                <?php if ($product->image_3) : ?>
                                    <div id="img-image_3">
                                        <img src="<?= base_url('images/product/' . $product->image_3) ?>" class="img-thumbnail mt-2" width="100" height="50" alt="image-3">
                                        <button class="btn btn-danger btn-sm delete-image" data-image="<?= $product->image_3 ?>" data-column="image_3" type="button"><i class="ci-trash"></i></button>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="col">
                            <div>
                                <label for="formFileLg" class="form-label">Image 4</label>
                                <input class="form-control " id="formFileLg" name="image_4" type="file">
                                <?= form_error('image_4', '<span class="text-danger">', '</span>'); ?>
                                <?php if ($product->image_4) : ?>
                                    <div id="img-image_4">
                                        <img src="<?= base_url('images/product/' . $product->image_4) ?>" class="img-thumbnail mt-2" width="100" height="50" alt="image-4">
                                        <button class="btn btn-danger btn-sm delete-image" data-image="<?= $product->image_4 ?>" data-column="image_4" type="button"><i class="ci-trash"></i></button>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="col">
                            <div>
                                <label for="formFileLg" class="form-label">Image 5</label>
                                <input class="form-control " id="formFileLg" name="image_5" type="file">
                                <?= form_error('image_5', '<span class="text-danger">', '</span>'); ?>
                                <?php if ($product->image_5) : ?>
                                    <div id="img-image_5">
                                        <img src="<?= base_url('images/product/' . $product->image_5) ?>" class="img-thumbnail mt-2" width="100" height="50" alt="image-5">
                                        <button class="btn btn-danger btn-sm delete-image" data-image="<?= $product->image_5 ?>" data-column="image_5" type="button"><i class="ci-trash"></i></button>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>

                    <?php foreach ($attributes as $attribute) : ?>
                        <input type="hidden" name="attribute_idE[]" value="<?= $attribute->id ?>">
                        <div class="row mb-2" id="row-<?= $attribute->id ?>">
                            <div class="col-md-3">
                                <div class="form-floating">
                                    <select class="form-select <?= form_error('colourE[]') ? 'is-invalid' : ''; ?>" name="colourE[]" aria-label="Floating label select example">
                                        <option value="" selected>Select Color</option>
                                        <?php foreach ($colours as $colour) : ?>
                                            <option value="<?= $colour->id ?>" <?= $colour->id == $attribute->colour_id ? 'selected' : '' ?>><?= $colour->name ?></option>
                                        <?php endforeach ?>
                                    </select>
                                    <label for="floatingSelect">Colour</label>
                                    <?= form_error('colourE[]', '<span class="text-danger">', '</span>'); ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-floating">
                                    <select class="form-select <?= form_error('sizeE[]') ? 'is-invalid' : ''; ?>" name="sizeE[]" aria-label="Floating label select example">
                                        <option value="" selected>Select Size</option>
                                        <?php foreach ($sizes as $size) : ?>
                                            <option value="<?= $size->id ?>" <?= $size->id == $attribute->size_id ? 'selected' : '' ?>><?= $size->name ?></option>
                                        <?php endforeach ?>
                                    </select>
                                    <label for="floatingSelect">Size</label>
                                    <?= form_error('sizeE[]', '<span class="text-danger">', '</span>'); ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-floating">
                                    <input type="text" name="qtyE[]" class="form-control <?= form_error('qtyE[]') ? 'is-invalid' : ''; ?>" value="<?= $attribute->qty; ?>" placeholder="Qty">
                                    <label for="floatingInputGrid">Qty</label>
                                    <?= form_error('qtyE[]', '<span class="text-danger">', '</span>'); ?>
                                </div>
                            </div>
                            <div class="col-md-3 mb-2">
                                <button class="btn btn-sm btn-danger remove-attribute py-3" data-id="<?= $attribute->id ?>" type="button">Remove</button>
                            </div>
                        </div>

                    <?php endforeach ?>

                    <div class="row mb-2 mt-5 attributes">
                        <div class="col-md-3">
                            <div class="form-floating">
                                <select class="form-select <?= form_error('colour[]') ? 'is-invalid' : ''; ?>" name="colour[]" aria-label="Floating label select example">
                                    <option value="" selected>Select Color</option>
                                    <?php foreach ($colours as $colour) : ?>
                                        <option value="<?= $colour->id ?>" <?= set_value('colour[]') == $colour->id ? 'selected' : '' ?>><?= $colour->name ?></option>
                                    <?php endforeach ?>
                                </select>
                                <label for="floatingSelect">Colour</label>
                                <?= form_error('colour[]', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-floating">
                                <select class="form-select <?= form_error('size[]') ? 'is-invalid' : ''; ?>" name="size[]" aria-label="Floating label select example">
                                    <option value="" selected>Select Size</option>
                                    <?php foreach ($sizes as $size) : ?>
                                        <option value="<?= $size->id ?>" <?= set_value('size[]') == $size->id ? 'selected' : '' ?>><?= $size->name ?></option>
                                    <?php endforeach ?>
                                </select>
                                <label for="floatingSelect">Size</label>
                                <?= form_error('size[]', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-floating">
                                <input type="text" name="qty[]" class="form-control <?= form_error('qty[]') ? 'is-invalid' : ''; ?>" value="<?= set_value('qty[]'); ?>" placeholder="Qty">
                                <label for="floatingInputGrid">Qty</label>
                                <?= form_error('qty[]', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="col-md-3 mb-2">
                            <button class="btn btn-sm btn-success add-attributes py-3" type="button">Add More</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md">
                            <button class="btn btn-sm btn-success float-end" type="submit">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <style>
        .vscomp-ele {
            max-width: none;
        }
    </style>
    <script>
        var CSRFToken = '<?= $this->security->get_csrf_token_name(); ?>';
        var CSRFHash = '<?= $this->security->get_csrf_hash(); ?>';
        $(document).ready(function() {
            subCategory();
            var counter = 1;

            $('.add-attributes').click(function(e) {
                e.preventDefault();
                let count = counter++;
                let attributes = `<div class="row" id="attribute-` + count + `">
                        <div class="col-md-3">
                            <div class="form-floating">
                                <select class="form-select" name="colour[]" aria-label="Floating label select example">
                                    <option value="" selected>Select Color</option>
                                    <?php foreach ($colours as $colour) : ?>
                                        <option value="<?= $colour->id ?>"><?= $colour->name ?></option>
                                    <?php endforeach  ?>
                                </select>
                                <label for="floatingSelect">Colour</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-floating">
                                <select class="form-select" name="size[]" aria-label="Floating label select example">
                                    <option value="" selected>Select Size</option>
                                    <?php foreach ($sizes as $size) : ?>
                                        <option value="<?= $size->id ?>"><?= $size->name ?></option>
                                    <?php endforeach  ?>
                                </select>
                                <label for="floatingSelect">Size</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-floating">
                                <input type="text" name="qty[]" class="form-control"  value="" placeholder="Qty">
                                <label for="floatingInputGrid">Qty</label>                                
                            </div>
                        </div>                        
                        <div class="col-md-3 mb-2">
                            <input type="button" class="btn btn-sm btn-danger py-3" onclick="remove(` + count + `)" value="Remove">
                        </div>
                    </div>`;
                $('.attributes').append(attributes);
            });


            $('#category_id').change(function() {
                subCategory();
            });

            $('.remove-attribute').click(function() {
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Do you want to delete this data?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Delete'
                }).then((result) => {
                    if (result.isConfirmed) {

                        $.ajax({
                            method: "POST",
                            url: "<?= base_url('admin/product/destroy_attribute') ?>",
                            dataType: 'json',
                            data: {
                                [CSRFToken]: CSRFHash,
                                id: id
                            },
                            success: function(response) {

                                if (response.status == 'success') {
                                    $('#row-' + id).remove();
                                    $.toast({
                                        heading: 'Success',
                                        text: response.message,
                                        icon: response.status
                                    });

                                } else {
                                    $.toast({
                                        heading: 'Error',
                                        text: response.message,
                                        icon: response.status
                                    });
                                }
                            }
                        })
                    }
                });
            });

            $('.delete-image').click(function() {
                let image = $(this).data('image');
                let column = $(this).data('column');
                let id = $('#id').val();
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Do you want to delete this image?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Delete'
                }).then((result) => {
                    if (result.isConfirmed) {

                        $.ajax({
                            method: "POST",
                            url: "<?= base_url('admin/product/destroy_image') ?>",
                            dataType: 'json',
                            data: {
                                [CSRFToken]: CSRFHash,
                                id: id,
                                image: image,
                                column: column
                            },
                            success: function(response) {

                                if (response.status == 'success') {
                                    $('#img-' + column).remove();
                                    $.toast({
                                        heading: 'Success',
                                        text: response.message,
                                        icon: response.status
                                    });

                                } else {
                                    $.toast({
                                        heading: 'Error',
                                        text: response.message,
                                        icon: response.status
                                    });
                                }
                            }
                        })
                    }
                });
            });
            


        });

        function subCategory() {
            var CSRFToken = '<?= $this->security->get_csrf_token_name(); ?>';
            var CSRFHash = '<?= $this->security->get_csrf_hash(); ?>';
            let category_id = $('#category_id').find(":selected").val();
            var data = {
                [CSRFToken]: CSRFHash,
                category_id: category_id,
                validation: '<?= set_value('subcategory_id') ?>'
            };
            $('#subcategory_id').attr('disabled', false);
            $.ajax({
                method: "POST",
                url: "<?= base_url('admin/product/getSubcategories') ?>",
                data: data,
                success: function(response) {
                    $('#subcategory_id').html(response);
                }
            })
        }

        function remove(id) {
            $('#attribute-' + id).remove();
        }
    </script>