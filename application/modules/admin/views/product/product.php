<div class="py-4">
    <div class="container">

        <div class="card">
            <div class="card-header fw-bold">
                All Category
                <a href="<?= base_url('admin/product/create')?>" class="btn btn-primary btn-sm float-end">Add New</a>
            </div>
            <div class="card-body">
                <div class="table-responsive-sm">
                    <table id="dataTable" class="table table-hover color-bordered-table info-bordered-table">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>CATEGORY</th>
                                <th>SUB-CATEGORY</th>
                                <th>PRODUCT</th>
                                <th>IMAGE</th>
                                <th>STATUS</th>
                                <th>CREATED DATE</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    $(function() {
        var CSRFToken = '<?= $this->security->get_csrf_token_name(); ?>';
        var CSRFHash = '<?= $this->security->get_csrf_hash(); ?>'; 
        var data = { [CSRFToken]: CSRFHash };

        // data table
        var dataTable = $('#dataTable').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                url: "<?= base_url("admin/product/table"); ?>",
                type: "POST",
                data: data
            },
            "columnDefs": [{
                "targets": [0,4,7],
                "orderable": false
            }]
        });

        // destroy
        $("#dataTable tbody").on('click', '.deleteButton', function() {
            var id = $(this).data('id');

            Swal.fire({
                title: 'Are you sure?',
                text: "Do you want to delete this data?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Delete'
            }).then((result) => {
                if (result.isConfirmed) {

                    $.ajax({
                        method: "POST",
                        url: "<?= base_url('admin/product/destroy') ?>",
                        dataType: 'json',
                        data: {
                            [CSRFToken]: CSRFHash,
                            id: id
                        },
                        success: function(response) {
                            if (response.status == 'success') {
                                $.toast({
                                    heading: 'Success',
                                    text: response.message,
                                    icon: response.status
                                });

                                dataTable.ajax.reload();

                            } else {
                                $.toast({
                                    heading: 'Error',
                                    text: response.message,
                                    icon: response.status
                                });
                            }
                        }
                    })
                }
            })

        });

        // change status
        $("#dataTable tbody").on('change', '.changeStatus', function() {
            var id = $(this).data('id');
            var status = $(this).data('status');

            $.ajax({
                method: "POST",
                url: "<?= base_url('admin/product/changeStatus') ?>",
                dataType: 'json',
                data: {
                    [CSRFToken]: CSRFHash,
                    id: id,
                    status: status
                },
                success: function(response) {
                    if (response.status == 'success') {
                        $.toast({
                            heading: 'Success',
                            text: response.message,
                            icon: response.status
                        });
                    } else if (response.status == 'warning') {
                        $.toast({
                            heading: 'Warning',
                            text: response.message,
                            icon: response.status
                        });
                    } else {
                        $.toast({
                            heading: 'Error',
                            text: response.message,
                            icon: response.status
                        });
                    }
                    dataTable.ajax.reload();
                }
            })
        });

    });
</script>