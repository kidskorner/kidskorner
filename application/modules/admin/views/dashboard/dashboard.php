<div class="container py-5 mb-lg-3">
  <div class="row justify-content-center">
    <div class="col-xl-12 col-lg-12">
      <div class="row">
        <div class="col-sm-3 mb-3"><a class="card h-100 border-0 shadow-sm" href="index-2.html">
            <div class="card-body">
              <div class="d-flex align-items-center"><i class="ci-home text-primary h4 mb-0"></i>
                <div class="ps-3">
                  <h5 class="fs-sm mb-0">Homepage</h5><span class="text-muted fs-ms">Return to homepage</span>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-3 mb-3"><a class="card h-100 border-0 shadow-sm" href="#">
            <div class="card-body">
              <div class="d-flex align-items-center"><i class="ci-search text-success h4 mb-0"></i>
                <div class="ps-3">
                  <h5 class="fs-sm mb-0">Search</h5><span class="text-muted fs-ms">Find with advanced search</span>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-3 mb-3"><a class="card h-100 border-0 shadow-sm" href="help-topics.html">
            <div class="card-body">
              <div class="d-flex align-items-center"><i class="ci-help text-info h4 mb-0"></i>
                <div class="ps-3">
                  <h5 class="fs-sm mb-0">Help &amp; Support</h5><span class="text-muted fs-ms">Visit our help center</span>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-3 mb-3"><a class="card h-100 border-0 shadow-sm" href="help-topics.html">
            <div class="card-body">
              <div class="d-flex align-items-center"><i class="ci-help text-info h4 mb-0"></i>
                <div class="ps-3">
                  <h5 class="fs-sm mb-0">Help &amp; Support</h5><span class="text-muted fs-ms">Visit our help center</span>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-3 mb-3"><a class="card h-100 border-0 shadow-sm" href="help-topics.html">
            <div class="card-body">
              <div class="d-flex align-items-center"><i class="ci-help text-info h4 mb-0"></i>
                <div class="ps-3">
                  <h5 class="fs-sm mb-0">Help &amp; Support</h5><span class="text-muted fs-ms">Visit our help center</span>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-3 mb-3"><a class="card h-100 border-0 shadow-sm" href="help-topics.html">
            <div class="card-body">
              <div class="d-flex align-items-center"><i class="ci-help text-info h4 mb-0"></i>
                <div class="ps-3">
                  <h5 class="fs-sm mb-0">Help &amp; Support</h5><span class="text-muted fs-ms">Visit our help center</span>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-3 mb-3"><a class="card h-100 border-0 shadow-sm" href="help-topics.html">
            <div class="card-body">
              <div class="d-flex align-items-center"><i class="ci-help text-info h4 mb-0"></i>
                <div class="ps-3">
                  <h5 class="fs-sm mb-0">Help &amp; Support</h5><span class="text-muted fs-ms">Visit our help center</span>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-3 mb-3"><a class="card h-100 border-0 shadow-sm" href="help-topics.html">
            <div class="card-body">
              <div class="d-flex align-items-center"><i class="ci-help text-info h4 mb-0"></i>
                <div class="ps-3">
                  <h5 class="fs-sm mb-0">Help &amp; Support</h5><span class="text-muted fs-ms">Visit our help center</span>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>