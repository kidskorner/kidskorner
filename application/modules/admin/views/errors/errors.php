<div class="container">
    <div class="card text-center mt-5">
        <div class="card-header">
            <h1 class="fw-bold">404</h1>
        </div>
        <div class="card-body p-5">
            <h5 class="card-title mt-5">Page Not Found !</h5>
            <p class="card-text">YOU SEEM TO BE TRYING TO FIND HIS WAY HOME</p>
            <a href="<?= base_url() ?>" class="btn btn-danger">Back to home</a>
        </div>
    </div>
</div>