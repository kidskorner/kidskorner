<div class="py-4">
    <div class="container">
        <div class="card">
            <div class="card-header fw-bold">
                Create Category
                <a href="<?= base_url('admin/category') ?>" class="btn btn-primary btn-sm float-end">Back</a>
            </div>
            <div class="card-body">
                <form action="<?= base_url('admin/category/store') ?>" method="POST" enctype="multipart/form-data" autocomplete="off">
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                    <div class="row g-2 mb-2">
                        <div class="col-md-6">
                            <div class="form-floating">
                                <input type="text" class="form-control <?= form_error('name') ? 'is-invalid' : ''; ?>" name="name" value="<?= set_value('name'); ?>" placeholder="name" >
                                <label for="floatingInputGrid">Category</label>
                                <?= form_error('name', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-2">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="formFileLg" class="form-label">Image</label>
                                <input class="form-control " id="formFileLg" name="image" type="file">
                                <?= form_error('image', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <button class="btn btn-sm btn-success float-end" type="submit">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>