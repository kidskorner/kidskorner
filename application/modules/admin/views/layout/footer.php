</main>
<!-- Footer-->
<footer class="footer bg-dark pt-1">   
    <div class="pt-1 bg-darker">
        <div class="container">            
            <div class="pb-4 fs-xs text-light opacity-50 text-center text-md-start">© All rights reserved.<a class="text-light" href="#" target="_blank" rel="noopener">KidsKorner</a></div>
        </div>
    </div>
</footer>
<!-- Vendor scrits: js libraries and plugins-->
<script src="<?= base_url('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/simplebar/dist/simplebar.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/tiny-slider/dist/min/tiny-slider.js') ?>"></script>
<script src="<?= base_url('assets/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery-datatable.js') ?>"></script>
<script src="<?= base_url('assets/js/datatables.min.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.toast.js') ?>"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- Main theme script-->
<script src="<?= base_url('assets/js/theme.min.js') ?>"></script>
</body>

</html>