<div class="py-4">
    <div class="container">
        <div class="card">
            <div class="card-header fw-bold">
                Create Banner
                <a href="<?= base_url('admin/banner') ?>" class="btn btn-primary btn-sm float-end">Back</a>
            </div>
            <div class="card-body">
                <form action="<?= base_url('admin/banner/store') ?>" method="POST" enctype="multipart/form-data" autocomplete="off">
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="form-floating">
                                <input type="text" class="form-control <?= form_error('title') ? 'is-invalid' : ''; ?>" name="title" value="<?= set_value('title'); ?>" placeholder="Title">
                                <label for="floatingInputGrid">Title</label>
                                <?= form_error('title', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <input type="text" class="form-control <?= form_error('price') ? 'is-invalid' : ''; ?>" name="price" value="<?= set_value('price'); ?>" placeholder="Price">
                                <label for="floatingInputGrid">Price</label>
                                <?= form_error('price', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                    </div>


                    <div class="form-floating mb-2">
                        <textarea class="form-control <?= form_error('short_description') ? 'is-invalid' : ''; ?>" name="short_description" placeholder="Short Description"><?= set_value('short_description'); ?></textarea>
                        <label for="floatingTextarea">Short Description</label>
                        <?= form_error('short_description', '<span class="text-danger">', '</span>'); ?>
                    </div>


                    <div class="row mb-2">

                        <div class="col-md-6">
                            <div class="form-floating">
                                <input type="text" class="form-control <?= form_error('link') ? 'is-invalid' : ''; ?>" name="link" value="<?= set_value('link'); ?>" placeholder="Link">
                                <label for="floatingInputGrid">Link</label>
                                <?= form_error('link', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div>
                                <!-- <label for="formFileLg" class="form-label">Image</label> -->
                                <input class="form-control" id="formFileLg" name="image" type="file">
                                <?= form_error('image', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md">
                            <button class="btn btn-sm btn-success float-end" type="submit">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>