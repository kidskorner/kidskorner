<?php
class Subcategory_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = 'subcategories';
    }

    public function getSubcategories($search, $order, $order_column, $order_dir, $start, $length, $draw)
    {

        $column = ["id", "name", "image", "is_active",  "created_at"];

        $query = "SELECT sc.*, c.name as category FROM $this->table sc INNER JOIN categories c ON c.id = sc.category_id";
        

        $query .= " WHERE ";


        if (isset($search)) {
            $query .= '(sc.name LIKE "%' . $search . '%")';
        }

        if (!empty($order)) {
            $query .= ' ORDER BY ' . "$column[$order_column]" . ' ' . $order_dir . '';
        } else {
            $query .= ' ORDER BY sc.id DESC ';
        }

        $query1 = '';
        if ($length != -1) {
            $query1 .= ' LIMIT ' . $start . ',' . $length;
        }

        $number_filter_rows = $this->db->query($query)->num_rows();
        $result = $this->db->query($query . $query1)->result();

        $i = 1;
        $data = array();
        foreach ($result as $row) {  // preparing an array
            $nestedData = array();
            $status = ($row->is_active == 1)? 'checked' : '';
            $nestedData[] = $i++;
            $nestedData[] = $row->category;
            $nestedData[] = $row->name;
            $nestedData[] = "<img src='" . base_url('images/subcategory/' . $row->image) . "' alt='Subcategory Image' width='50' height='25'>";
            $nestedData[] = "<div class='checkbox checkbox-success'><input class='changeStatus'  type='checkbox' $status   name='status' data-id='".$row->id."' data-status='".$row->is_active."' ><label></label></div>";
            $nestedData[] = date('d-M-Y h:i a', strtotime($row->created_at));
            $nestedData[] = "<a href='" . base_url('admin/subcategory/edit/' . $row->id . '') . "' class='me-3' title='Edit'><i class='h5 ci-edit-alt'></i></a>
                            <a href='javascript:void(0)' class='m-r-5 deleteButton' title='Delete' data-id='".$row->id."'><i class='h5 ci-trash'></i></a>";

            $data[] = $nestedData;
        }
        
        $query2 = "SELECT sc.*, c.name as category FROM $this->table sc INNER JOIN categories c ON c.id = sc.category_id";
        $total_rows = $this->db->query($query2)->num_rows();

        $output = array(
            "draw" => intval($draw),
            "recordsTotal" => $total_rows,
            "recordsFiltered" => $number_filter_rows,
            "data" => $data
        );

        return $output;
    }

    public function store($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function getSubcategory($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function update($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return true;
    }

    public function destroy($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
        return true;
    }

    public function checkExist($categoryId, $name) 
    {
        $this->db->where('name', $name);
        $this->db->where('category_id', $categoryId);        
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

    public function checkExistEdit($categoryId, $id, $name) 
    {
        $this->db->where('name', $name);
        $this->db->where('category_id', $categoryId); 
        $this->db->where_not_in('id', $id);        
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

    public function getCategories()
    {
        $this->db->select('id, name');
        $this->db->where('is_active', 1);
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get('categories');
        return $query->result();
    }
    
}
