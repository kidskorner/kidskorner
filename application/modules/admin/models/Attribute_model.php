<?php
class Attribute_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = 'attributes';
    }

    

    public function store($data)
    {
        $this->db->insert_batch($this->table, $data);
        return $this->db->insert_id();
    }

    public function getProduct($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function update($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return true;
    }

    public function destroy($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
        return true;
    }

    public function destroyByProduct($id)
    {
        $this->db->where('product_id', $id);
        $this->db->delete($this->table);
        return true;
    }

    public function checkExist($categoryId, $subcategoryId, $name) 
    {
        $this->db->where('name', $name);
        $this->db->where('category_id', $categoryId);  
        $this->db->where('subcategory_id', $subcategoryId);      
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

    public function checkExistEdit($categoryId, $subcategoryId, $id, $name) 
    {
        $this->db->where('name', $name);
        $this->db->where('category_id', $categoryId); 
        $this->db->where('subcategory_id', $subcategoryId); 
        $this->db->where_not_in('id', $id);        
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }
    
    public function getAttributes($id)
    {
        $this->db->where('product_id', $id);
        $query = $this->db->get($this->table);
        return $query->result();
    }
}
