<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Banner extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->controller = strtolower(__CLASS__);
		$this->load->model('Banner_model', 'banner');
	}

	public function index()
	{
		$this->layout->template(TEMPLATE_ADMIN)->show("{$this->controller}/{$this->controller}");
	}

	public function create()
	{
		$this->layout->template(TEMPLATE_ADMIN)->show("{$this->controller}/" . __FUNCTION__);
	}

	public function store()
	{
		$this->form_validation->set_rules('title', 'Title', 'required|min_length[3]|max_length[100]|is_unique[banners.title]');
		$this->form_validation->set_rules('short_description', 'Short Description', 'required|min_length[3]|max_length[500]');
		$this->form_validation->set_rules('price', 'Price', 'required|numeric');
		$this->form_validation->set_rules('link', 'Link', 'required|valid_url');
		$this->form_validation->set_rules('image', '', 'callback_fileCheck');
		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data['title'] = $this->security->xss_clean($this->input->post('title'));
			$slug = $this->security->xss_clean(url_title($data['title'], 'dash', true));
			$data['short_description'] = $this->input->post('short_description');
			$data['price'] = $this->input->post('price');
			$data['link'] = $this->input->post('link');
			
            
			$ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
			$image = $slug . '.' . $ext;
			$uplPath = FCPATH . 'images/banner/';
			$uploadPath = $uplPath . $image;
			move_uploaded_file($_FILES['image']['tmp_name'], $uploadPath);

			$data['image'] = $image;

			$insertId = $this->banner->store($data);
			if ($insertId) {
				$this->session->set_flashdata('success', 'Data saved successfully.');
				return redirect("admin/{$this->controller}");
			} else {
				$this->session->set_flashdata('error', 'Data insertion failed, please try again!');
				return redirect($this->create());
			}
		}
	}

	public function edit($id)
	{
		$data['banner'] = $this->banner->getBanner($id);
		$this->layout->template(TEMPLATE_ADMIN)->show("{$this->controller}/" . __FUNCTION__, $data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$eImage = $this->input->post('eimage');
		
		$this->form_validation->set_rules('title', 'Title', 'required|min_length[3]|max_length[100]|callback_checkExist');
        $this->form_validation->set_rules('short_description', 'Short Description', 'required|min_length[3]|max_length[500]');
		$this->form_validation->set_rules('price', 'Price', 'required|numeric');
		$this->form_validation->set_rules('link', 'Link', 'required|valid_url');

		if ($_FILES['image']['name'] != '') {
			$this->form_validation->set_rules('image', '', 'callback_fileCheck');
		}

		if ($this->form_validation->run() == FALSE) {
			$this->edit($id);
		} else {
			$data['title'] = $this->security->xss_clean($this->input->post('title'));
			$slug = $this->security->xss_clean(url_title($data['title'], 'dash', true));
			$data['short_description'] = $this->input->post('short_description');
			$data['price'] = $this->input->post('price');
			$data['link'] = $this->input->post('link');
			
			if ($_FILES['image']['name'] != '') {
				// remove old image
				unlink(FCPATH . 'images/banner/' . $eImage);

				$ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
				$image = $slug . '.' . $ext;
				$uplPath = FCPATH . 'images/banner/';
				$uploadPath = $uplPath . $image;
				move_uploaded_file($_FILES['image']['tmp_name'], $uploadPath);

				$data['image'] = $image;
			}
			$update = $this->banner->update($data, $id);
			if ($update) {
				$this->session->set_flashdata('success', 'Data updated successfully.');
				return redirect("admin/{$this->controller}");
			} else {
				$this->session->set_flashdata('error', 'Data updation failed, please try again!');
				return redirect($this->edit($id));
			}
		}
	}

	public function destroy()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$id = $this->input->get('id');
			$data = $this->banner->getBanner($id);
			unlink(FCPATH . 'images/banner/' . $data->image);
			$destroy = $this->banner->destroy($id);
			if ($destroy) {
				echo json_encode(['status' => 'success', 'message' => 'Data deleted successfully.']);
			} else {
				echo json_encode(['status' => 'error', 'message' => 'Somthing went wrong']);
			}
		}
	}

	public function table()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$search = $_GET['search']['value'];

			if (isset($_GET['order'])) {
				$order = $_GET['order'];
			} else {
				$order = '';
			}

			if (isset($_GET['order']['0']['column'])) {
				$order_column = $_GET['order']['0']['column'];
			} else {
				$order_column = '';
			}

			if (isset($_GET['order']['0']['dir'])) {
				$order_dir = $_GET['order']['0']['dir'];
			} else {
				$order_dir = '';
			}

			$start = $_GET['start'];
			$length = $_GET['length'];

			$draw = $_GET['draw'];

			$banners = $this->banner->getBanners($search, $order, $order_column, $order_dir, $start, $length, $draw);
			echo json_encode($banners);
		}
	}

	public function changeStatus()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$id = $this->input->get('id');
			$status = $this->input->get('status');
			if ($status == 1) {
				$statusValue = 0;
				$message = 'Deactivated successfully.';
				$newStatus = 'warning';
			} else {
				$statusValue = 1;
				$message = 'Activated successfully.';
				$newStatus = 'success';
			}
			$changeStatus = $this->banner->update(['is_active' => $statusValue], $id);
			if ($changeStatus) {
				echo json_encode(['status' => $newStatus, 'message' => $message]);
			} else {
				echo json_encode(['status' => 'error', 'message' => 'Somthing went wrong']);
			}
		}
	}


	/*
     * file value and type check during validation
     */
	public function fileCheck($str)
	{
		$allowed_mime_type_arr = ['jpeg', 'png', 'webp'];
		$ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
		if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != "") {
			if (in_array($ext, $allowed_mime_type_arr)) {
				if ($_FILES['image']['size'] > 1048576) {
					$this->form_validation->set_message('fileCheck', 'Please upload below 1MB file.');
					return false;
				}
				$image_info = getimagesize($_FILES['image']['tmp_name']);
				// if ($image_info[0] > 520 || $image_info[1] > 520 || $image_info[0] < 220 || $image_info[1] < 220) {
				// 	$this->form_validation->set_message('fileCheck', 'Please upload min [width:220 height:220] and max[width:520 height:520] image.');
				// 	return false;
				// }
				return true;
			} else {
				$this->form_validation->set_message('fileCheck', 'Please select only jpg/png file.');
				return false;
			}
		} else {
			$this->form_validation->set_message('fileCheck', 'Please upload a banner image to upload.');
			return false;
		}
	}

	/*
     * check exist during validation
     */
	function checkExist($name)
	{
		$id = $this->input->post('id');
		$result = $this->banner->checkExist($id, $name);
		if ($result == 0)
			return true;
		else {
			$this->form_validation->set_message('checkExist', 'Banner name must be unique');
			return false;
		}
	}
}
