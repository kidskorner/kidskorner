<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Colour extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->controller = strtolower(__CLASS__);
		$this->load->model('Colour_model', 'colour');
	}

	public function index()
	{
		$this->layout->template(TEMPLATE_ADMIN)->show("{$this->controller}/{$this->controller}");
	}

	public function create()
	{
		$this->layout->template(TEMPLATE_ADMIN)->show("{$this->controller}/" . __FUNCTION__);
	}

	public function store()
	{
		$this->form_validation->set_rules('name', 'Colour Name', 'required|min_length[3]|max_length[50]|is_unique[colours.name]');
		
		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data['name'] = $this->security->xss_clean($this->input->post('name'));
			$slug = $this->security->xss_clean(url_title($data['name'], 'dash', true));
			$data['slug'] = $slug;
            
			$insertId = $this->colour->store($data);
			if ($insertId) {
				$this->session->set_flashdata('success', 'Data saved successfully.');
				return redirect($this->controller);
			} else {
				$this->session->set_flashdata('error', 'Data insertion failed, please try again!');
				return redirect($this->create());
			}
		}
	}

	public function edit($id)
	{
		$data['colour'] = $this->colour->getColour($id);
		$this->layout->template(TEMPLATE_ADMIN)->show("{$this->controller}/" . __FUNCTION__, $data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		
		$this->form_validation->set_rules('name', 'Colour Name', 'required|min_length[3]|max_length[50]|callback_checkExist');
        
		if ($this->form_validation->run() == FALSE) {
			$this->edit($id);
		} else {
			$data['name'] = $this->security->xss_clean($this->input->post('name'));
			$slug = $this->security->xss_clean(url_title($data['name'], 'dash', true));
			$data['slug'] = $slug;
						
			$update = $this->colour->update($data, $id);
			if ($update) {
				$this->session->set_flashdata('success', 'Data updated successfully.');
				return redirect($this->controller);
			} else {
				$this->session->set_flashdata('error', 'Data updation failed, please try again!');
				return redirect($this->edit($id));
			}
		}
	}

	public function destroy()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$id = $this->input->get('id');					
			$destroy = $this->colour->destroy($id);
			if ($destroy) {
				echo json_encode(['status' => 'success', 'message' => 'Data deleted successfully.']);
			} else {
				echo json_encode(['status' => 'error', 'message' => 'Somthing went wrong']);
			}
		}
	}

	public function table()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$search = $_GET['search']['value'];

			if (isset($_GET['order'])) {
				$order = $_GET['order'];
			} else {
				$order = '';
			}

			if (isset($_GET['order']['0']['column'])) {
				$order_column = $_GET['order']['0']['column'];
			} else {
				$order_column = '';
			}

			if (isset($_GET['order']['0']['dir'])) {
				$order_dir = $_GET['order']['0']['dir'];
			} else {
				$order_dir = '';
			}

			$start = $_GET['start'];
			$length = $_GET['length'];

			$draw = $_GET['draw'];

			$colours = $this->colour->getColours($search, $order, $order_column, $order_dir, $start, $length, $draw);
			echo json_encode($colours);
		}
	}

	public function changeStatus()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$id = $this->input->get('id');
			$status = $this->input->get('status');
			if ($status == 1) {
				$statusValue = 0;
				$message = 'Deactivated successfully.';
				$newStatus = 'warning';
			} else {
				$statusValue = 1;
				$message = 'Activated successfully.';
				$newStatus = 'success';
			}
			$changeStatus = $this->colour->update(['is_active' => $statusValue], $id);
			if ($changeStatus) {
				echo json_encode(['status' => $newStatus, 'message' => $message]);
			} else {
				echo json_encode(['status' => 'error', 'message' => 'Somthing went wrong']);
			}
		}
	}


	/*
     * check exist during validation
     */
	function checkExist($name)
	{
		$id = $this->input->post('id');
		$result = $this->colour->checkExist($id, $name);
		if ($result == 0)
			return true;
		else {
			$this->form_validation->set_message('checkExist', 'Colour name must be unique');
			return false;
		}
	}
}
