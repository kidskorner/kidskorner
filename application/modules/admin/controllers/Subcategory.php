<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Subcategory extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->controller = strtolower(__CLASS__);
		$this->load->model('Subcategory_model', 'subcategory');
	}

	public function index()
	{
		$this->layout->template(TEMPLATE_ADMIN)->show("{$this->controller}/{$this->controller}");
	}

	public function create()
	{
		$data['categories'] = $this->subcategory->getCategories();
		$this->layout->template(TEMPLATE_ADMIN)->show("{$this->controller}/" . __FUNCTION__, $data);
	}

	public function store()
	{
		$this->form_validation->set_rules('category_id', 'Category', 'required');
		$this->form_validation->set_rules('name', 'Sub Category Name', 'required|min_length[3]|max_length[50]|callback_checkExist');
		$this->form_validation->set_rules('image', '', 'callback_fileCheck');
		if ($this->form_validation->run() == FALSE) {
			
			$this->create();
		} else {
			$data['category_id'] = $this->input->post('category_id');
			$data['name'] = $this->security->xss_clean($this->input->post('name'));
			$slug = $this->security->xss_clean(url_title($data['name'], 'dash', true));

            
			$ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
			$image = $slug . '.' . $ext;
			$uplPath = FCPATH . 'images/subcategory/';
			$uploadPath = $uplPath . $image;
			move_uploaded_file($_FILES['image']['tmp_name'], $uploadPath);

			$data['image'] = $image;

			$insertId = $this->subcategory->store($data);
			if ($insertId) {
				$this->session->set_flashdata('success', 'Data saved successfully.');
				return redirect("admin/{$this->controller}");
			} else {
				$this->session->set_flashdata('error', 'Data insertion failed, please try again!');
				return redirect("admin/{$this->create()}");
			}
		}
	}

	public function edit($id)
	{
		$data['categories'] = $this->subcategory->getCategories();
		$data['subcategory'] = $this->subcategory->getSubcategory($id);
		$this->layout->template(TEMPLATE_ADMIN)->show("{$this->controller}/" . __FUNCTION__, $data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$eImage = $this->input->post('eimage');

		$this->form_validation->set_rules('category_id', 'Category', 'required');
		$this->form_validation->set_rules('name', 'Sub Category Name', 'required|min_length[3]|max_length[50]|callback_checkExistEdit');
        
		if ($_FILES['image']['name'] != '') {
			$this->form_validation->set_rules('image', '', 'callback_fileCheck');
		}

		if ($this->form_validation->run() == FALSE) {
			$this->edit($id);
		} else {
			$data['category_id'] = $this->input->post('category_id');
			$data['name'] = $this->security->xss_clean($this->input->post('name'));
			$slug = $this->security->xss_clean(url_title($data['name'], 'dash', true));
           $data['slug'] = $slug;

			if ($_FILES['image']['name'] != '') {
				// remove old image
				unlink(FCPATH . 'images/subcategory/' . $eImage);

				$ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
				$image = $slug . '.' . $ext;
				$uplPath = FCPATH . 'images/subcategory/';
				$uploadPath = $uplPath . $image;
				move_uploaded_file($_FILES['image']['tmp_name'], $uploadPath);

				$data['image'] = $image;
			}
			$update = $this->subcategory->update($data, $id);
			if ($update) {
				$this->session->set_flashdata('success', 'Data updated successfully.');
				return redirect("admin/{$this->controller}");
			} else {
				$this->session->set_flashdata('error', 'Data updation failed, please try again!');
				return redirect("admin/{$this->edit($id)}");
			}
		}
	}

	public function destroy()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$id = $this->input->get('id');
			$data = $this->subcategory->getSubcategory($id);
			unlink(FCPATH . 'images/subcategory/' . $data->image);
			$destroy = $this->subcategory->destroy($id);
			if ($destroy) {
				echo json_encode(['status' => 'success', 'message' => 'Data deleted successfully.']);
			} else {
				echo json_encode(['status' => 'error', 'message' => 'Somthing went wrong']);
			}
		}
	}

	public function table()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$search = $_GET['search']['value'];

			if (isset($_GET['order'])) {
				$order = $_GET['order'];
			} else {
				$order = '';
			}

			if (isset($_GET['order']['0']['column'])) {
				$order_column = $_GET['order']['0']['column'];
			} else {
				$order_column = '';
			}

			if (isset($_GET['order']['0']['dir'])) {
				$order_dir = $_GET['order']['0']['dir'];
			} else {
				$order_dir = '';
			}

			$start = $_GET['start'];
			$length = $_GET['length'];

			$draw = $_GET['draw'];

			$subcategories = $this->subcategory->getSubcategories($search, $order, $order_column, $order_dir, $start, $length, $draw);
			echo json_encode($subcategories);
		}
	}

	public function changeStatus()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$id = $this->input->get('id');
			$status = $this->input->get('status');
			if ($status == 1) {
				$statusValue = 0;
				$message = 'Deactivated successfully.';
				$newStatus = 'warning';
			} else {
				$statusValue = 1;
				$message = 'Activated successfully.';
				$newStatus = 'success';
			}
			$changeStatus = $this->subcategory->update(['is_active' => $statusValue], $id);
			if ($changeStatus) {
				echo json_encode(['status' => $newStatus, 'message' => $message]);
			} else {
				echo json_encode(['status' => 'error', 'message' => 'Somthing went wrong']);
			}
		}
	}


	/*
     * file value and type check during validation
     */
	public function fileCheck($str)
	{
		$allowed_mime_type_arr = ['image/jpeg', 'image/png'];
		$mime = get_mime_by_extension($_FILES['image']['name']);
		if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != "") {
			if (in_array($mime, $allowed_mime_type_arr)) {
				if ($_FILES['image']['size'] > 1048576) {
					$this->form_validation->set_message('fileCheck', 'Please upload below 1MB file.');
					return false;
				}
				$image_info = getimagesize($_FILES['image']['tmp_name']);
				// if ($image_info[0] > 520 || $image_info[1] > 520 || $image_info[0] < 220 || $image_info[1] < 220) {
				// 	$this->form_validation->set_message('fileCheck', 'Please upload min [width:220 height:220] and max[width:520 height:520] image.');
				// 	return false;
				// }
				return true;
			} else {
				$this->form_validation->set_message('fileCheck', 'Please select only jpg/png file.');
				return false;
			}
		} else {
			$this->form_validation->set_message('fileCheck', 'Please choose a sub category image to upload.');
			return false;
		}
	}

	/*
     * check exist during validation
     */
	function checkExist($name)
	{
		$categoryId = $this->input->post('category_id');
		$result = $this->subcategory->checkExist($categoryId, $name);
		if ($result == 0)
			return true;
		else {
			$this->form_validation->set_message('checkExist', 'Sub category name must be unique under category');
			return false;
		}
	}

	function checkExistEdit($name)
	{
		$id = $this->input->post('id');
		$categoryId = $this->input->post('category_id');
		$result = $this->subcategory->checkExistEdit($categoryId, $id, $name);
		if ($result == 0)
			return true;
		else {
			$this->form_validation->set_message('checkExistEdit', 'Sub category name must be unique under category');
			return false;
		}
	}
}
