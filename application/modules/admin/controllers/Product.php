<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->controller = strtolower(__CLASS__);
		$this->load->model('Product_model', 'product');
		$this->load->model('Attribute_model', 'attribute');
	}

	public function index()
	{
		$this->layout->template(TEMPLATE_ADMIN)->show("{$this->controller}/{$this->controller}");
	}

	public function create()
	{
		$data['categories'] = $this->product->getCategories();
		$data['subcategories'] = $this->product->getSubcategories($category_id = '');
		$data['colours'] = $this->product->getColours();
		$data['sizes'] = $this->product->getSizes();
		$this->layout->template(TEMPLATE_ADMIN)->show("{$this->controller}/" . __FUNCTION__, $data);
	}

	public function store()
	{
		// d($this->input->post());
		$this->form_validation->set_rules('category_id', 'Category', 'required');
		$this->form_validation->set_rules('subcategory_id', 'Subcategory', 'required');
		$this->form_validation->set_rules('name', 'Product Name', 'required|min_length[3]|max_length[50]|callback_checkExist');
		$this->form_validation->set_rules('sku', 'sku', 'required|min_length[3]|max_length[50]|is_unique[products.sku]');
		$this->form_validation->set_rules('regular_price', 'Regular Price', 'required|numeric');
		$this->form_validation->set_rules('sale_price', 'Sale Price', 'required|numeric|less_than[regular_price]');
		$this->form_validation->set_rules('short_description', 'Short Description', 'required');
		$this->form_validation->set_rules('description', 'description', 'required');
		$this->form_validation->set_rules('colour[]', 'Colour', 'required');
		$this->form_validation->set_rules('size[]', 'Size', 'required');
		$this->form_validation->set_rules('qty[]', 'Qty', 'required');
		$this->form_validation->set_rules('image_1', 'Image 1', 'callback_fileCheck');

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {

			$data['category_id'] = $this->input->post('category_id');
			$data['subcategory_id'] = $this->input->post('subcategory_id');
			$data['name'] = $this->security->xss_clean($this->input->post('name'));
			$slug = $this->security->xss_clean(url_title($data['name'], 'dash', true));
			$data['slug'] = $slug;
			$data['sku'] = $this->input->post('sku');
			$data['regular_price'] = $this->input->post('regular_price');
			$data['sale_price'] = $this->input->post('sale_price');
			$data['short_description'] = $this->input->post('short_description');
			$data['description'] = $this->input->post('description');


			if ($_FILES['image_1']['name'] != '') {
				$ext = pathinfo($_FILES['image_1']['name'], PATHINFO_EXTENSION);
				$image = $slug . '-1.' . $ext;
				$uplPath = FCPATH . 'images/product/';
				$uploadPath = $uplPath . $image;
				move_uploaded_file($_FILES['image_1']['tmp_name'], $uploadPath);
				$data['image_1'] = $image;
			}

			if ($_FILES['image_2']['name'] != '') {
				$ext = pathinfo($_FILES['image_2']['name'], PATHINFO_EXTENSION);
				$image = $slug . '-2.' . $ext;
				$uplPath = FCPATH . 'images/product/';
				$uploadPath = $uplPath . $image;
				move_uploaded_file($_FILES['image_2']['tmp_name'], $uploadPath);
				$data['image_2'] = $image;
			}

			if ($_FILES['image_3']['name'] != '') {
				$ext = pathinfo($_FILES['image_3']['name'], PATHINFO_EXTENSION);
				$image = $slug . '-3.' . $ext;
				$uplPath = FCPATH . 'images/product/';
				$uploadPath = $uplPath . $image;
				move_uploaded_file($_FILES['image_3']['tmp_name'], $uploadPath);
				$data['image_3'] = $image;
			}

			if ($_FILES['image_4']['name'] != '') {
				$ext = pathinfo($_FILES['image_4']['name'], PATHINFO_EXTENSION);
				$image = $slug . '-4.' . $ext;
				$uplPath = FCPATH . 'images/product/';
				$uploadPath = $uplPath . $image;
				move_uploaded_file($_FILES['image_4']['tmp_name'], $uploadPath);
				$data['image_4'] = $image;
			}

			if ($_FILES['image_5']['name'] != '') {
				$ext = pathinfo($_FILES['image_5']['name'], PATHINFO_EXTENSION);
				$image = $slug . '-5.' . $ext;
				$uplPath = FCPATH . 'images/product/';
				$uploadPath = $uplPath . $image;
				move_uploaded_file($_FILES['image_5']['tmp_name'], $uploadPath);
				$data['image_5'] = $image;
			}

			$productId = $this->product->store($data);

			if ($productId) {

				$attributeInsertData = [];
				$colour = $this->input->post('colour');
				$size = $this->input->post('size');
				$qty = $this->input->post('qty');

				for ($i = 0; $i < count($colour); $i++) {
					$attributeData = [
						'product_id' => $productId,
						'colour_id' => $colour[$i],
						'size_id' => $size[$i],
						'qty' => $qty[$i]
					];
					array_push($attributeInsertData, $attributeData);
				}


				$attributeId = $this->attribute->store($attributeInsertData);

				if ($attributeId) {
					$this->session->set_flashdata('success', 'Data saved successfully.');
					return redirect("admin/{$this->controller}");
				} else {
					$this->product->destroy($productId);
					$this->attribute->destroyByProduct($productId);
					$this->session->set_flashdata('error', 'Data insertion failed, please try again!');
					return redirect("admin/{$this->create()}");
				}
			} else {
				$this->session->set_flashdata('error', 'Data insertion failed, please try again!');
				return redirect("admin/{$this->create()}");
			}
		}
	}

	public function edit($id)
	{
		$data['product'] = $this->product->getProduct($id);
		$data['categories'] = $this->product->getCategories();
		$data['subcategories'] = $this->product->getSubcategories($data['product']->category_id);
		$data['colours'] = $this->product->getColours();
		$data['sizes'] = $this->product->getSizes();
		$data['attributes'] = $this->attribute->getAttributes($id);
		
		$this->layout->template(TEMPLATE_ADMIN)->show("{$this->controller}/" . __FUNCTION__, $data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$eImage_1 = $this->input->post('eimage_1');
		$eImage_2 = $this->input->post('eimage_2');
		$eImage_3 = $this->input->post('eimage_3');
		$eImage_4 = $this->input->post('eimage_4');
		$eImage_5 = $this->input->post('eimage_5');

		$this->form_validation->set_rules('category_id', 'Category', 'required');
		$this->form_validation->set_rules('subcategory_id', 'Subcategory', 'required');
		$this->form_validation->set_rules('name', 'Product Name', 'required|min_length[3]|max_length[50]|callback_checkExistEdit');
		$this->form_validation->set_rules('sku', 'sku', 'required|min_length[3]|max_length[50]|callback_checkExistEditSku[products.sku]');
		$this->form_validation->set_rules('regular_price', 'Regular Price', 'required|numeric');
		$this->form_validation->set_rules('sale_price', 'Sale Price', 'required|numeric|less_than[regular_price]');
		$this->form_validation->set_rules('short_description', 'Short Description', 'required');
		$this->form_validation->set_rules('description', 'description', 'required');
		$this->form_validation->set_rules('colourE[]', 'Colour', 'required');
		$this->form_validation->set_rules('sizeE[]', 'Size', 'required');
		$this->form_validation->set_rules('qtyE[]', 'Qty', 'required');
		
		if ($_FILES['image_1']['name'] != '') {
			$this->form_validation->set_rules('image', '', 'callback_fileCheck1');
		}

		if ($_FILES['image_2']['name'] != '') {
			$this->form_validation->set_rules('image', '', 'callback_fileCheck2');
		}

		if ($_FILES['image_3']['name'] != '') {
			$this->form_validation->set_rules('image', '', 'callback_fileCheck3');
		}

		if ($_FILES['image_4']['name'] != '') {
			$this->form_validation->set_rules('image', '', 'callback_fileCheck4');
		}

		if ($_FILES['image_5']['name'] != '') {
			$this->form_validation->set_rules('image', '', 'callback_fileCheck5');
		}

		if ($this->form_validation->run() == FALSE) {
			$this->edit($id);
		} else {
			$data['category_id'] = $this->input->post('category_id');
			$data['name'] = $this->security->xss_clean($this->input->post('name'));
			$slug = $this->security->xss_clean(url_title($data['name'], 'dash', true));
			$data['slug'] = $slug;

			if ($_FILES['image_1']['name'] != '') {
				// remove old image
				unlink(FCPATH . 'images/product/' . $eImage_1);

				$ext = pathinfo($_FILES['image_1']['name'], PATHINFO_EXTENSION);
				$image = $slug . date('Ymdhis') . '_1.' . $ext;
				$uplPath = FCPATH . 'images/product/';
				$uploadPath = $uplPath . $image;
				move_uploaded_file($_FILES['image_1']['tmp_name'], $uploadPath);

				$data['image_1'] = $image;
			}

			if ($_FILES['image_2']['name'] != '') {
				// remove old image
				unlink(FCPATH . 'images/product/' . $eImage_2);

				$ext = pathinfo($_FILES['image_2']['name'], PATHINFO_EXTENSION);
				$image = $slug . date('Ymdhis') . '_2.' . $ext;
				$uplPath = FCPATH . 'images/product/';
				$uploadPath = $uplPath . $image;
				move_uploaded_file($_FILES['image_2']['tmp_name'], $uploadPath);

				$data['image_2'] = $image;
			}

			if ($_FILES['image_3']['name'] != '') {
				// remove old image
				unlink(FCPATH . 'images/product/' . $eImage_3);

				$ext = pathinfo($_FILES['image_3']['name'], PATHINFO_EXTENSION);
				$image = $slug . date('Ymdhis') . '_3.' . $ext;
				$uplPath = FCPATH . 'images/product/';
				$uploadPath = $uplPath . $image;
				move_uploaded_file($_FILES['image_3']['tmp_name'], $uploadPath);

				$data['image_3'] = $image;
			}

			if ($_FILES['image_4']['name'] != '') {
				// remove old image
				unlink(FCPATH . 'images/product/' . $eImage_4);

				$ext = pathinfo($_FILES['image_4']['name'], PATHINFO_EXTENSION);
				$image = $slug . date('Ymdhis') . '_4.' . $ext;
				$uplPath = FCPATH . 'images/product/';
				$uploadPath = $uplPath . $image;
				move_uploaded_file($_FILES['image_4']['tmp_name'], $uploadPath);

				$data['image_4'] = $image;
			}

			if ($_FILES['image_5']['name'] != '') {
				// remove old image
				unlink(FCPATH . 'images/product/' . $eImage_5);

				$ext = pathinfo($_FILES['image_5']['name'], PATHINFO_EXTENSION);
				$image = $slug . date('Ymdhis') . '_5.' . $ext;
				$uplPath = FCPATH . 'images/product/';
				$uploadPath = $uplPath . $image;
				move_uploaded_file($_FILES['image_5']['tmp_name'], $uploadPath);

				$data['image_5'] = $image;
			}

			$update = $this->product->update($data, $id);
			if ($update) {

				$colourE = $this->input->post('colourE');
				$sizeE = $this->input->post('sizeE');
				$qtyE = $this->input->post('qtyE');
				$attribute_idE = $this->input->post('attribute_idE');

				for ($j = 0; $j < count($colourE); $j++) {
					$attribute_id = $attribute_idE[$j];
					$attributeData = [
						'product_id' => $id,
						'colour_id' => $colourE[$j],
						'size_id' => $sizeE[$j],
						'qty' => $qtyE[$j]
					];
				$this->attribute->update($attributeData, $attribute_id);
				}


				// new attribute insert
				$attributeInsertData = [];
				$colour = $this->input->post('colour');
				$size = $this->input->post('size');
				$qty = $this->input->post('qty');

				for ($i = 0; $i < count($colour); $i++) {
					if($colour[$i]!='' && $size[$i]!='' && $qty[$i]!=0){
						$attributeData = [
							'product_id' => $id,
							'colour_id' => $colour[$i],
							'size_id' => $size[$i],
							'qty' => $qty[$i]
						];
						array_push($attributeInsertData, $attributeData);
					}					
				}
				
				if(count($attributeInsertData)>0){
					$attributeId = $this->attribute->store($attributeInsertData);
				}
				

				// if ($attributeId) {
				// 	$this->session->set_flashdata('success', 'Data saved successfully.');
				// 	return redirect("admin/{$this->controller}");
				// } else {
				// 	$this->product->destroy($productId);
				// 	$this->attribute->destroyByProduct($productId);
				// 	$this->session->set_flashdata('error', 'Data insertion failed, please try again!');
				// 	return redirect("admin/{$this->controller}/{$this->create()}");
				// }

				$this->session->set_flashdata('success', 'Data updated successfully.');
				return redirect("admin/{$this->controller}");
			} else {
				$this->session->set_flashdata('error', 'Data updation failed, please try again!');
				return redirect("admin/{$this->controller}/{$this->edit($id)}");
			}
		}
	}

	public function destroy()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$id = $this->input->post('id');
			$data = $this->product->getProduct($id);
			
			if($data->image_1){
				unlink(FCPATH . 'images/product/' . $data->image_1);
			}

			if($data->image_2){
				unlink(FCPATH . 'images/product/' . $data->image_2);
			}

			if($data->image_3){
				unlink(FCPATH . 'images/product/' . $data->image_3);
			}

			if($data->image_4){
				unlink(FCPATH . 'images/product/' . $data->image_4);
			}

			if($data->image_5){
				unlink(FCPATH . 'images/product/' . $data->image_5);
			}

						
			$destroy = $this->product->destroy($id);
			if ($destroy) {
				echo json_encode(['status' => 'success', 'message' => 'Data deleted successfully.']);
			} else {
				echo json_encode(['status' => 'error', 'message' => 'Somthing went wrong']);
			}
		}
	}

	public function destroy_attribute()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$id = $this->input->post('id');									
			$destroy = $this->attribute->destroy($id);
			if ($destroy) {
				echo json_encode(['status' => 'success', 'message' => 'Data deleted successfully.']);
			} else {
				echo json_encode(['status' => 'error', 'message' => 'Somthing went wrong']);
			}
		}
	}

	public function destroy_image()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$id = $this->input->post('id');
			$image = $this->input->post('image');			
			$column = $this->input->post('column');

			unlink(FCPATH . 'images/product/' . $image);
						
			$update = $this->product->update(["$column" => NULL], $id);
			if ($update) {
				echo json_encode(['status' => 'success', 'message' => 'Data deleted successfully.']);
			} else {
				echo json_encode(['status' => 'error', 'message' => 'Somthing went wrong']);
			}
		}
	}

	public function table()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$search = $_POST['search']['value'];

			if (isset($_POST['order'])) {
				$order = $_POST['order'];
			} else {
				$order = '';
			}

			if (isset($_POST['order']['0']['column'])) {
				$order_column = $_POST['order']['0']['column'];
			} else {
				$order_column = '';
			}

			if (isset($_POST['order']['0']['dir'])) {
				$order_dir = $_POST['order']['0']['dir'];
			} else {
				$order_dir = '';
			}

			$start = $_POST['start'];
			$length = $_POST['length'];

			$draw = $_POST['draw'];

			$products = $this->product->getProducts($search, $order, $order_column, $order_dir, $start, $length, $draw);
			echo json_encode($products);
		}
	}

	public function changeStatus()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$id = $this->input->post('id');
			$status = $this->input->post('status');
			if ($status == 1) {
				$statusValue = 0;
				$message = 'Deactivated successfully.';
				$newStatus = 'warning';
			} else {
				$statusValue = 1;
				$message = 'Activated successfully.';
				$newStatus = 'success';
			}
			$changeStatus = $this->product->update(['is_active' => $statusValue], $id);
			if ($changeStatus) {
				echo json_encode(['status' => $newStatus, 'message' => $message]);
			} else {
				echo json_encode(['status' => 'error', 'message' => 'Somthing went wrong']);
			}
		}
	}

	public function getSubcategories()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$category_id = $this->input->post('category_id');
			$validation = $this->input->post('validation');
			$subcategories = $this->product->getSubcategories($category_id);
			$data = '<option value="">Select Subcategory</option>';
			if ($subcategories) {
				foreach ($subcategories as $subcategory) {
					$status = ($validation == $subcategory->id) ? 'selected' : '';
					$data .= '<option value="'. $subcategory->id .'" '.$status.' >' . $subcategory->name . '</option>';
				}
			}
			echo $data;
			exit;
		}
	}


	/*
     * file value and type check during validation
     */
	public function fileCheck1($str)
	{
		$allowed_mime_type_arr = ['image/jpeg', 'image/png'];
		$mime = get_mime_by_extension($_FILES['image_1']['name']);
		if (isset($_FILES['image_1']['name']) && $_FILES['image_1']['name'] != "") {
			if (in_array($mime, $allowed_mime_type_arr)) {
				if ($_FILES['image_1']['size'] > 1048576) {
					$this->form_validation->set_message('fileCheck', 'Please upload below 1MB file.');
					return false;
				}
				$image_info = getimagesize($_FILES['image_1']['tmp_name']);
				// if ($image_info[0] > 520 || $image_info[1] > 520 || $image_info[0] < 220 || $image_info[1] < 220) {
				// 	$this->form_validation->set_message('fileCheck', 'Please upload min [width:220 height:220] and max[width:520 height:520] image.');
				// 	return false;
				// }
				return true;
			} else {
				$this->form_validation->set_message('fileCheck', 'Please select only jpg/png file.');
				return false;
			}
		} else {
			$this->form_validation->set_message('fileCheck', 'Please choose a product image to upload.');
			return false;
		}
	}

	public function fileCheck2($str)
	{
		$allowed_mime_type_arr = ['image/jpeg', 'image/png'];
		$mime = get_mime_by_extension($_FILES['image_2']['name']);
		if (isset($_FILES['image_2']['name']) && $_FILES['image_2']['name'] != "") {
			if (in_array($mime, $allowed_mime_type_arr)) {
				if ($_FILES['image_2']['size'] > 1048576) {
					$this->form_validation->set_message('fileCheck', 'Please upload below 1MB file.');
					return false;
				}
				$image_info = getimagesize($_FILES['image_2']['tmp_name']);
				// if ($image_info[0] > 520 || $image_info[1] > 520 || $image_info[0] < 220 || $image_info[1] < 220) {
				// 	$this->form_validation->set_message('fileCheck', 'Please upload min [width:220 height:220] and max[width:520 height:520] image.');
				// 	return false;
				// }
				return true;
			} else {
				$this->form_validation->set_message('fileCheck', 'Please select only jpg/png file.');
				return false;
			}
		} else {
			$this->form_validation->set_message('fileCheck', 'Please choose a product image to upload.');
			return false;
		}
	}

	public function fileCheck3($str)
	{
		$allowed_mime_type_arr = ['image/jpeg', 'image/png'];
		$mime = get_mime_by_extension($_FILES['image_3']['name']);
		if (isset($_FILES['image_3']['name']) && $_FILES['image_3']['name'] != "") {
			if (in_array($mime, $allowed_mime_type_arr)) {
				if ($_FILES['image_3']['size'] > 1048576) {
					$this->form_validation->set_message('fileCheck', 'Please upload below 1MB file.');
					return false;
				}
				$image_info = getimagesize($_FILES['image_3']['tmp_name']);
				// if ($image_info[0] > 520 || $image_info[1] > 520 || $image_info[0] < 220 || $image_info[1] < 220) {
				// 	$this->form_validation->set_message('fileCheck', 'Please upload min [width:220 height:220] and max[width:520 height:520] image.');
				// 	return false;
				// }
				return true;
			} else {
				$this->form_validation->set_message('fileCheck', 'Please select only jpg/png file.');
				return false;
			}
		} else {
			$this->form_validation->set_message('fileCheck', 'Please choose a product image to upload.');
			return false;
		}
	}

	public function fileCheck4($str)
	{
		$allowed_mime_type_arr = ['image/jpeg', 'image/png'];
		$mime = get_mime_by_extension($_FILES['image_4']['name']);
		if (isset($_FILES['image_4']['name']) && $_FILES['image_4']['name'] != "") {
			if (in_array($mime, $allowed_mime_type_arr)) {
				if ($_FILES['image_4']['size'] > 1048576) {
					$this->form_validation->set_message('fileCheck', 'Please upload below 1MB file.');
					return false;
				}
				$image_info = getimagesize($_FILES['image_4']['tmp_name']);
				// if ($image_info[0] > 520 || $image_info[1] > 520 || $image_info[0] < 220 || $image_info[1] < 220) {
				// 	$this->form_validation->set_message('fileCheck', 'Please upload min [width:220 height:220] and max[width:520 height:520] image.');
				// 	return false;
				// }
				return true;
			} else {
				$this->form_validation->set_message('fileCheck', 'Please select only jpg/png file.');
				return false;
			}
		} else {
			$this->form_validation->set_message('fileCheck', 'Please choose a product image to upload.');
			return false;
		}
	}

	public function fileCheck5($str)
	{
		$allowed_mime_type_arr = ['image/jpeg', 'image/png'];
		$mime = get_mime_by_extension($_FILES['image_5']['name']);
		if (isset($_FILES['image_5']['name']) && $_FILES['image_5']['name'] != "") {
			if (in_array($mime, $allowed_mime_type_arr)) {
				if ($_FILES['image_5']['size'] > 1048576) {
					$this->form_validation->set_message('fileCheck', 'Please upload below 1MB file.');
					return false;
				}
				$image_info = getimagesize($_FILES['image_5']['tmp_name']);
				// if ($image_info[0] > 520 || $image_info[1] > 520 || $image_info[0] < 220 || $image_info[1] < 220) {
				// 	$this->form_validation->set_message('fileCheck', 'Please upload min [width:220 height:220] and max[width:520 height:520] image.');
				// 	return false;
				// }
				return true;
			} else {
				$this->form_validation->set_message('fileCheck', 'Please select only jpg/png file.');
				return false;
			}
		} else {
			$this->form_validation->set_message('fileCheck', 'Please choose a product image to upload.');
			return false;
		}
	}

	/*
     * check exist during validation
     */
	function checkExist($name)
	{
		$categoryId = $this->input->post('category_id');
		$subcategoryId = $this->input->post('subcategory_id');
		$result = $this->product->checkExist($categoryId, $subcategoryId, $name);
		if ($result == 0)
			return true;
		else {
			$this->form_validation->set_message('checkExist', 'Product name must be unique under categories');
			return false;
		}
	}

	function checkExistEdit($name)
	{
		$id = $this->input->post('id');
		$categoryId = $this->input->post('category_id');
		$subcategoryId = $this->input->post('subcategory_id');

		$result = $this->product->checkExistEdit($categoryId, $subcategoryId, $id, $name);
		if ($result == 0)
			return true;
		else {
			$this->form_validation->set_message('checkExistEdit', 'Product name must be unique under categories');
			return false;
		}
	}

	function checkExistEditSku($sku)
	{
		$id = $this->input->post('id');
		
		$result = $this->product->checkExistEditSku($id, $sku);
		if ($result == 0)
			return true;
		else {
			$this->form_validation->set_message('checkExistEditSku', 'Product SKU must be unique');
			return false;
		}
	}
}
