<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Subcategory extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->controller = strtolower(__CLASS__);
		$this->method = strtolower(__FUNCTION__);

		$this->load->model('Home_model', 'home');
	}

	public function index()
	{
		
		$subcategory_slug = $this->uri->segment(2);		
		$subcategory_id = $this->home->subcategory_by_slug($subcategory_slug);

		if ($subcategory_id) {
			$data['categories'] = $this->home->categories();
			$data['colours'] = $this->home->colours();
			$data['sizes'] = $this->home->sizes();
			$data['best_offer_products'] = $this->home->best_offer_products();
			$data['new_arrival_products'] = $this->home->new_arrival_products();

			$this->layout->template(TEMPLATE_USER)->show("{$this->controller}/{$this->controller}", $data);
			
		} else {
			redirect('errors');
		}
	}

	
}
