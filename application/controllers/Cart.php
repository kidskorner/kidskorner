<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cart extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->controller = strtolower(__CLASS__);		
		$this->load->model('User_model', 'user');
		$this->user_id = $this->session->userdata('user_id');		
	}
	

	public function add_to_wishlist()
	{
		if ($this->input->is_ajax_request()) {

			if($this->user_id){
				$product_id = $this->input->post('product_id');
				$exist = $this->home->exist_wishlist($product_id, $this->user_id);
	
			   echo json_encode($exist);
			}else{
				echo json_encode("no user");
			}
			

        } else {
            exit('No direct script access allowed');
        }
	}
}
