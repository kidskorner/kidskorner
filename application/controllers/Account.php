<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Account extends CI_Controller
{
	var $data;

	public function __construct()
	{
		parent::__construct();
		$this->controller = strtolower(__CLASS__);
		if (!isUser()) {
			redirect(base_url());
		}
		$this->load->model('User_model', 'user');
		$this->user_id = $this->session->userdata('user_id');

		$this->data = array(
            'user' => $this->user->user_details($this->user_id),
			'wishlist_count' => $this->wishlist_count(),
			'cart_count' => $this->cart_count()          
        );

		
	}

	public function orders()
	{
		$data = $this->data;
		$this->layout->template(TEMPLATE_USER)->show("{$this->controller}/" . __FUNCTION__, $data);
	}

	public function wishlist()
	{
		$data = $this->data;
		$data['wishlists'] = $this->user->wishlists($this->user_id);
		$this->layout->template(TEMPLATE_USER)->show("{$this->controller}/" . __FUNCTION__, $data);
	}

	public function profile()
	{
		$data = $this->data;
		$this->layout->template(TEMPLATE_USER)->show("{$this->controller}/" . __FUNCTION__, $data);
	}

	public function addresses()
	{
		$data = $this->data;
		$this->layout->template(TEMPLATE_USER)->show("{$this->controller}/" . __FUNCTION__, $data);
	}

	public function cart()
	{
		$data = $this->data;
		$data['carts'] = $this->user->cart_items($this->user_id);
		$this->layout->template(TEMPLATE_USER)->show("{$this->controller}/" . __FUNCTION__, $data);
	}


	public function add_to_wishlist()
	{
		if ($this->input->is_ajax_request()) {

			$product_id = $this->input->post('product_id');
			$exist = $this->user->exist_wishlist($product_id, $this->user_id);

			if($exist<1){
				$wishlist_id = $this->user->add_to_wishlist(['product_id' => $product_id, 'user_id' => $this->user_id]);
				if($wishlist_id){
					echo json_encode(['status' => 'success', 'wishlist_count' => $this->wishlist_count(), 'message' => 'Item added to wishlist.']);
					exit;
				}else{
					echo json_encode(['status' => 'error', 'message' => 'Something went wrong.']);
					exit;
				}
			}else{
				$this->user->remove_from_wishlist($product_id, $this->user_id);
				echo json_encode(['status' => 'info', 'message' => 'Item removed from wishlist.']);
				exit;
			}

		} else {
			exit('No direct script access allowed');
		}
	}

	public function add_to_cart()
	{
		if ($this->input->is_ajax_request()) {

			$product_id = $this->input->post('product_id');
			$colour_id = $this->input->post('colour_id');
			$size_id = $this->input->post('size_id');
			$qty = $this->input->post('qty');

			$cart = $this->user->exist_cart($this->user_id, $product_id, $colour_id, $size_id);

			if($cart){
				if($cart->qty + $qty > 5){
					echo json_encode(['status' => 'warning', 'message' => 'Maximum item quantity in cart.']);
					exit;
				}

				$this->user->update_cart_qty($cart->id, $cart->qty, $qty);
				echo json_encode(['status' => 'info', 'message' => 'Item quantity increased.', 'cart_count' => $this->cart_count()]);
				exit;
				
			}else{

				$cart_id = $this->user->add_to_cart(['user_id' => $this->user_id, 'product_id' => $product_id, 'colour_id' => $colour_id, 'size_id' => $size_id, 'qty' => $qty ]);
				if($cart_id){
					echo json_encode(['status' => 'success', 'cart_count' => $this->cart_count(), 'message' => 'Item added to cart.']);
					exit;
				}else{
					echo json_encode(['status' => 'error', 'message' => 'Something went wrong.']);
					exit;
				}
			}

		} else {
			exit('No direct script access allowed');
		}
	}

	public function remove_from_wishlist()
	{
		if ($this->input->is_ajax_request()) {

			$wishlist_id = $this->input->post('wishlist_id');
			$status = $this->user->delete_from_wishlist($wishlist_id);
			
			if($status){
				echo json_encode(['status' => 'success', 'wishlist_count' => $this->wishlist_count(), 'message' => 'Item removed from wishlist.']);
				exit;
			}else{
				echo json_encode(['status' => 'error', 'message' => 'Something went wrong.']);
				exit;
			}

		} else {
			exit('No direct script access allowed');
		}
	}

	public function remove_from_cart()
	{
		if ($this->input->is_ajax_request()) {

			$cart_id = $this->input->post('cart_id');
			$status = $this->user->delete_from_cart($cart_id);
			
			if($status){
				echo json_encode(['status' => 'success', 'cart_count' => $this->cart_count(), 'cart_amount' => cartAmount(), 'message' => 'Item removed from cart.']);
				exit;
			}else{
				echo json_encode(['status' => 'error', 'message' => 'Something went wrong.']);
				exit;
			}

		} else {
			exit('No direct script access allowed');
		}
	}

	public function wishlist_count()
	{
		return $this->user->wishlist_count($this->user_id);
	}

	public function cart_count()
	{
		return $this->user->cart_count($this->user_id);
	}


}
