<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->controller = strtolower(__CLASS__);
		$this->method = strtolower(__FUNCTION__);

		$this->load->model('Home_model', 'home');
	}

	public function index()
	{		
		$data['banners'] = $this->home->banners();		
		$data['trending_products'] = $this->home->trending_products();
		$data['boy_products'] = $this->home->boy_products();
		$data['girl_products'] = $this->home->girl_products();
		$data['best_offer_products'] = $this->home->best_offer_products();
		$data['new_arrival_products'] = $this->home->new_arrival_products();

		$this->layout->template(TEMPLATE_USER)->show("{$this->controller}/{$this->controller}", $data);
	}

	
}
