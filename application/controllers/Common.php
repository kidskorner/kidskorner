<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Common extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->controller = strtolower(__CLASS__);
		$this->method = strtolower(__FUNCTION__);
		$this->load->library('ajax_pagination');
		$this->load->model('Home_model', 'home');
		$this->perPage = 3;  
	}	

	function ajax_product_pagination(){ 
		if($this->input->is_ajax_request()){	

			$category_slug = $this->input->post('category_slug');
			$category_id = $this->home->category_by_slug($category_slug);

			
			$subcategory_slug = $this->input->post('subcategory_slug');
			$subcategory_id = null;	
			if(isset($subcategory_slug)){
				$subcategory_id = $this->home->subcategory_by_slug($subcategory_slug);
			}	
			
			
			// Define offset 
			$page = $this->input->post('page'); 
			if(!$page){ 
				$offset = 0; 
			}else{ 
				$offset = $page; 
			} 
			 
			 
			$colour_id = $this->input->post('colour_id'); 
			$size_id = $this->input->post('size_id'); 
			$sort_by = $this->input->post('sort_by'); 
			
			$totalRec = count($this->home->products_by_filters('', '', $category_id, $subcategory_id, $sort_by, $colour_id, $size_id));
					
			$config['target']      = '#dataList'; 
			$config['base_url']    = base_url("$category_slug/$subcategory_slug/ajax_pagination"); 
			$config['total_rows']  = $totalRec; 
			$config['per_page']    = $this->perPage; 
			$config['link_func']   = 'searchFilter'; 
			 
			$this->ajax_pagination->initialize($config); 
							
			$data['category_slug'] = $category_slug;
			$data['subcategory_slug'] = $subcategory_slug;
			
			
			$data['products'] = $this->home->products_by_filters($offset, $this->perPage, $category_id, $subcategory_id, $sort_by, $colour_id, $size_id);
			// d($this->db->last_query());
			$this->load->view('common/ajax-data', $data, false); 
		}else{
			exit('No direct script access allowed');
		}
		
    } 

	public function sort_cookie()
	{
		if($this->input->is_ajax_request()){
			$sort_by = $this->input->post('sort_by');
			set_cookie('sort_by', $sort_by, 86400);			
		}else{
			exit('No direct script access allowed');
		}		
	}

	public function colour_cookie()
	{
		if($this->input->is_ajax_request()){

			$colour_id = $this->input->post('colour_id');
			$colour_ids = get_cookie('colour_id');

			if($colour_ids){
				$colour_array = explode(',', $colour_ids);
				if (in_array($colour_id, $colour_array)) {
					$new_colour_array = array_diff($colour_array, array($colour_id));
					set_cookie('colour_id', implode(',', $new_colour_array), 86400);
				}else{
					array_push($colour_array, $colour_id);
					set_cookie('colour_id', implode(',', $colour_array), 86400);
				}
			}else{				
				set_cookie('colour_id', $colour_id, 86400);
			}
						
		}else{
			exit('No direct script access allowed');
		}		
	}


	public function size_cookie()
	{
		if($this->input->is_ajax_request()){

			$size_id = $this->input->post('size_id');
			$size_ids = get_cookie('size_id');			

			if($size_ids){
				$size_array = explode(',', $size_ids);
				if (in_array($size_id, $size_array)) {
					$new_size_array = array_diff($size_array, array($size_id));
					set_cookie('size_id', implode(',', $new_size_array), 86400);
				}else{
					array_push($size_array, $size_id);
					set_cookie('size_id', implode(',', $size_array), 86400);
				}
			}else{				
				set_cookie('size_id', $size_id, 86400);
			}
						
		}else{
			exit('No direct script access allowed');
		}		
	}
}
