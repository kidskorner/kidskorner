
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->controller = strtolower(__CLASS__);

        $this->load->model('User_model', 'user');
    }


    public function sign_in()
    {
        if ($this->input->is_ajax_request()) {

            $phone = $this->input->post('phone');
            $otp = rand(1001, 9999);


            $exist = $this->user->exist_phone($phone);
            if ($exist) {

                $this->user->update_otp(['otp' => $otp], $exist->id);
                $this->session->set_userdata(['otp' => $otp, 'phone' => $phone]);
                $sms = sendOTP($phone, $otp);
                echo json_encode(['status' => 'otp']);
                exit;
            } else {

                $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric|exact_length[10]|is_unique[users.phone]');

                if ($this->form_validation->run() == FALSE) {

                    $errors['phone'] = form_error('phone', '<span class="text-danger phone">', '</span>');
                    echo json_encode(['status' => $errors]);
                } else {

                    $user_id = $this->user->sign_up(['phone' => $phone, 'otp' => $otp]);

                    if ($user_id) {
                        $sms = sendOTP($phone, $otp);
                        $this->session->set_userdata(['otp' => $otp, 'phone' => $phone]);
                        echo json_encode(['status' => 'otp']);
                        exit;
                    } else {
                        echo json_encode(['status' => 'error', 'message' => 'Something went wrong..']);
                    }
                }
            }
        } else {
            exit('No direct script access allowed');
        }
    }


    public function verify_otp()
    {
        if ($this->input->is_ajax_request()) {

            $first = $this->input->post('first');
            $second = $this->input->post('second');
            $third = $this->input->post('third');
            $fourth = $this->input->post('fourth');

            if ($first == '' || $second == '' || $third == '' || $fourth == '') {

                $errors['otp'] = "<span class='text-danger otp'>Please enter 4 digit OTP</span>";
                echo json_encode(['status' => $errors]);
                exit;
            } else {
                $exist = $this->user->exist_phone($this->session->userdata('phone'));
                if ($this->session->userdata('otp') == $first . $second . $third . $fourth) {
                    $this->session->unset_userdata(['otp', 'phone']);
                    $this->session->set_userdata(['user_id' => $exist->id]);
                    echo json_encode(['status' => 'match']);
                    exit;
                } else {
                    $errors['otp'] = "<span class='text-danger otp'>Invalid OTP</span>";
                    echo json_encode(['status' => $errors]);
                    exit;
                }
            }
        } else {
            exit('No direct script access allowed');
        }
    }


    public function resend()
    {
        if ($this->input->is_ajax_request()) {

            $otp = rand(1001, 9999);
            $phone = $this->session->userdata('phone');
            $sms = sendOTP($phone, $otp);
            $this->session->set_userdata(['otp' => $otp]);
            if($sms){
                echo json_encode(['status' => 'sent']);
                exit;
            }else{
                echo json_encode(['status' => 'faild']);
                exit;
            }
           
        } else {
            exit('No direct script access allowed');
        }
    }


    function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }
}
