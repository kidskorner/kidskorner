<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->controller = strtolower(__CLASS__);
		$this->method = strtolower(__FUNCTION__);
		$this->load->helper('cookie');
		$this->load->model('Home_model', 'home');
		
	}

	public function index()
	{
		$category_slug = $this->uri->segment(1);
		$subcategory_slug = $this->uri->segment(2);
		$product_slug = $this->uri->segment(3);

		$category_id = $this->home->category_by_slug($category_slug);
		$subcategory_id = $this->home->subcategory_by_slug($subcategory_slug);
		$data['product'] = $this->home->product_by_slug($product_slug);		
		
		$data['related_products'] = $this->home->related_products($category_id, $subcategory_id, $data['product']->id);
		$data['colours'] = $this->home->colours_by_product($data['product']->id);
		$data['sizes'] = $this->home->sizes_by_product($data['product']->id);
		// d($data['sizes']);
		if($category_id && $subcategory_id && $data['product']){
			
			// set_cookie('product_id', $data['product']->id);
			$this->layout->template(TEMPLATE_USER)->show("{$this->controller}/{$this->controller}", $data);

		}else{
			redirect('errors');
		}
		
		
	}

	
}
