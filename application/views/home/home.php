 <!-- Hero slider-->
 <section class="mb-4 mb-lg-5">
     <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
         <div class="carousel-inner">
             <?php foreach ($banners as $banner) : ?>
                 <div class="carousel-item active">
                     <img src="<?= base_url('images/banner/' . $banner->image) ?>" class="d-block w-100" alt="<?= $banner->title ?>">
                     <div class="carousel-caption d-none d-md-block">
                         <h5><?= $banner->title ?></h5>
                         <p><?= $banner->short_description ?></p>
                         <a href="<?= $banner->link ?>" class="btn btn-sm btn-danger">shop now</a>
                     </div>
                 </div>
             <?php endforeach ?>
         </div>
         <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
             <span class="carousel-control-prev-icon" aria-hidden="true"></span>
             <span class="visually-hidden">Previous</span>
         </button>
         <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
             <span class="carousel-control-next-icon" aria-hidden="true"></span>
             <span class="visually-hidden">Next</span>
         </button>
     </div>
 </section>
 <section class="container pt-md-3 pb-5 mb-md-3">
     <h2 class="h3 text-center">Trending products</h2>
     <div class="row pt-4 mx-n2">
         <!-- Product-->
         <?php foreach ($trending_products as $product) : ?>
             <div class="col-lg-3 col-md-4 col-sm-6 px-2 mb-4">
                 <div class="card product-card shadow-lg">
                     <button class="btn-wishlist btn-sm pro-<?= $product->id ?> add-to-wishlist <?= checkWishlist($product->id, $this->session->userdata('user_id')) ? 'wishlisted' : '' ?>" data-product-id="<?= $product->id ?>" type="button" data-bs-toggle="tooltip" data-bs-placement="left" title="Wishlist"><i class="ci-heart"></i></button>
                     <a class="card-img-top d-block overflow-hidden" href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>">
                         <img src="<?= base_url('images/product/' . $product->image_1) ?>" alt="<?= $product->name ?>">
                     </a>
                     <div class="card-body py-2">
                         <a class="product-meta d-block fs-xs pb-1" href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>"><?= str_replace('-', '', $product->subcategory) ?></a>
                         <h3 class="product-title fs-sm">
                             <a href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>"><?= $product->name ?></a>
                         </h3>
                         <div class="d-flex justify-content-between">
                             <div class="product-price"><span class="text-accent">₹ <?= $product->sale_price ?></span></div>
                             <!-- <div class="star-rating"><i class="star-rating-icon ci-star-filled active"></i><i class="star-rating-icon ci-star-filled active"></i><i class="star-rating-icon ci-star-filled active"></i><i class="star-rating-icon ci-star-half active"></i><i class="star-rating-icon ci-star"></i></div> -->
                         </div>
                     </div>
                     <div class="card-body">
                         <button class="btn btn-primary btn-sm d-block w-100 mb-2" type="button"><i class="ci-cart fs-sm me-1"></i>Add to Cart</button>
                         <!-- <div class="text-center"><a class="nav-link-style fs-ms" href="#quick-view" data-bs-toggle="modal"><i class="ci-eye align-middle me-1"></i>Quick view</a></div> -->
                     </div>
                 </div>
                 <hr class="d-sm-none">
             </div>
         <?php endforeach ?>
 </section>



 <div class="container py-5 my-md-3">
     <h2 class="h3 text-center pb-4">You may like for your boy</h2>
     <div class="tns-carousel tns-controls-static tns-controls-outside">
         <div class="tns-carousel-inner" data-carousel-options="{&quot;items&quot;: 2, &quot;controls&quot;: true, &quot;nav&quot;: false, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;500&quot;:{&quot;items&quot;:2, &quot;gutter&quot;: 18},&quot;768&quot;:{&quot;items&quot;:3, &quot;gutter&quot;: 20}, &quot;1100&quot;:{&quot;items&quot;:4, &quot;gutter&quot;: 30}}}">
             <!-- Product-->
             <?php foreach ($boy_products as $product) : ?>
                 <div>
                     <div class="card product-card card-static border">
                         <button class="btn-wishlist btn-sm pro-<?= $product->id ?> add-to-wishlist <?= checkWishlist($product->id, $this->session->userdata('user_id')) ? 'wishlisted' : '' ?>" data-product-id="<?= $product->id ?>" type="button" data-bs-toggle="tooltip" data-bs-placement="left" title="Wishlist"><i class="ci-heart"></i></button>
                         <a class="card-img-top d-block overflow-hidden" href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>">
                             <img src="<?= base_url('images/product/' . $product->image_1) ?>" alt="<?= $product->name ?>">
                         </a>
                         <div class="card-body py-2">
                             <a class="product-meta d-block fs-xs pb-1" href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>"><?= str_replace('-', ' ', $product->subcategory) ?></a>
                             <h3 class="product-title fs-sm"><a href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>"><?= $product->name ?></a></h3>
                             <div class="d-flex justify-content-between">
                                 <div class="product-price"><span class="text-accent">₹ <?= $product->sale_price ?></span></div>
                                 <!-- <div class="star-rating"><i class="star-rating-icon ci-star-filled active"></i><i class="star-rating-icon ci-star-filled active"></i><i class="star-rating-icon ci-star-filled active"></i><i class="star-rating-icon ci-star-half active"></i><i class="star-rating-icon ci-star"></i></div> -->
                             </div>
                         </div>
                         <div class="card-body">
                             <button class="btn btn-primary btn-sm d-block w-100 mb-2" type="button"><i class="ci-cart fs-sm me-1"></i>Add to Cart</button>
                         </div>
                     </div>
                 </div>
             <?php endforeach ?>
         </div>
     </div>
 </div>

 <!-- Banners-->
 <section class="container pb-4 mb-md-3">
     <div class="row">
         <div class="col-md-8 mb-4">
             <div class="d-sm-flex justify-content-between align-items-center bg-secondary overflow-hidden rounded-3">
                 <div class="py-4 my-2 my-md-0 py-md-5 px-4 ms-md-3 text-center text-sm-start">
                     <h4 class="fs-lg fw-light mb-2">Hurry up! Limited time offer</h4>
                     <h3 class="mb-4">Converse All Star on Sale</h3><a class="btn btn-primary btn-shadow btn-sm" href="#">Shop Now</a>
                 </div><img class="d-block ms-auto" src="<?= base_url('assets/img/shop/catalog/banner.jpg') ?>" alt="Shop Converse">
             </div>
         </div>
         <div class="col-md-4 mb-4">
             <div class="d-flex flex-column h-100 justify-content-center bg-size-cover bg-position-center rounded-3" style="background-image: url(assets/img/blog/banner-bg.jpg);">
                 <div class="py-4 my-2 px-4 text-center">
                     <div class="py-1">
                         <h5 class="mb-2">Your Add Banner Here</h5>
                         <p class="fs-sm text-muted">Hurry up to reserve your spot</p><a class="btn btn-primary btn-shadow btn-sm" href="#">Contact us</a>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </section>

 <div class="container py-5 my-md-3">
     <h2 class="h3 text-center pb-4">You may like for your girl</h2>
     <div class="tns-carousel tns-controls-static tns-controls-outside">
         <div class="tns-carousel-inner" data-carousel-options="{&quot;items&quot;: 2, &quot;controls&quot;: true, &quot;nav&quot;: false, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;500&quot;:{&quot;items&quot;:2, &quot;gutter&quot;: 18},&quot;768&quot;:{&quot;items&quot;:3, &quot;gutter&quot;: 20}, &quot;1100&quot;:{&quot;items&quot;:4, &quot;gutter&quot;: 30}}}">
             <!-- Product-->
             <?php foreach ($girl_products as $product) : ?>
                 <div>
                     <div class="card product-card card-static border">
                         <button class="btn-wishlist btn-sm pro-<?= $product->id ?> add-to-wishlist <?= checkWishlist($product->id, $this->session->userdata('user_id')) ? 'wishlisted' : '' ?>" data-product-id="<?= $product->id ?>" type="button" data-bs-toggle="tooltip" data-bs-placement="left" title="Wishlist"><i class="ci-heart"></i></button>
                         <a class="card-img-top d-block overflow-hidden" href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>">
                             <img src="<?= base_url('images/product/' . $product->image_1) ?>" alt="<?= $product->name ?>">
                         </a>
                         <div class="card-body py-2">
                             <a class="product-meta d-block fs-xs pb-1" href="<?= base_url($product->category . '/' . $product->subcategory) ?>"><?= str_replace('-', ' ', $product->subcategory) ?></a>
                             <h3 class="product-title fs-sm"><a href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>"><?= $product->name ?></a></h3>
                             <div class="d-flex justify-content-between">
                                 <div class="product-price"><span class="text-accent">₹ <?= $product->sale_price ?></span></div>
                                 <!-- <div class="star-rating"><i class="star-rating-icon ci-star-filled active"></i><i class="star-rating-icon ci-star-filled active"></i><i class="star-rating-icon ci-star-filled active"></i><i class="star-rating-icon ci-star-half active"></i><i class="star-rating-icon ci-star"></i></div> -->
                             </div>
                         </div>
                         <div class="card-body">
                             <button class="btn btn-primary btn-sm d-block w-100 mb-2" type="button"><i class="ci-cart fs-sm me-1"></i>Add to Cart</button>
                         </div>
                     </div>
                 </div>
             <?php endforeach ?>
         </div>
     </div>
 </div>

 <!-- Product widgets-->
 <section class="container pt-md-3 pb-4 pb-md-5 mb-lg-2">
     <div class="row">
         <div class="col-lg-4 col-md-6 mb-2 py-3">
             <div class="widget">
                 <h3 class="widget-title">Best Offers</h3>
                 <?php foreach ($best_offer_products as $product) : ?>
                     <div class="d-flex align-items-center pb-2 border-bottom">
                         <a class="d-block" href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>">
                             <img src="<?= base_url('images/product/' . $product->image_1) ?>" width="64" alt="<?= $product->name ?>">
                         </a>
                         <div class="ps-2">
                             <h6 class="widget-product-title"><a href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>"><?= $product->name ?></a></h6>
                             <div class="widget-product-meta"><span class="text-accent me-2">₹ <?= $product->sale_price ?></span></div>
                         </div>
                     </div>
                 <?php endforeach ?>
             </div>
         </div>
         <div class="col-lg-4 col-md-6 mb-2 py-3">
             <div class="widget">
                 <h3 class="widget-title">New arrivals</h3>
                 <?php foreach ($new_arrival_products as $product) : ?>
                     <div class="d-flex align-items-center pb-2 border-bottom"><a class="d-block" href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>">
                             <img src="<?= base_url('images/product/' . $product->image_1) ?>" width="64" alt="<?= $product->name ?>">
                         </a>
                         <div class="ps-2">
                             <h6 class="widget-product-title"><a href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>"><?= $product->name ?></a></h6>
                             <div class="widget-product-meta"><span class="text-accent me-2">₹ <?= $product->sale_price ?></span></div>
                         </div>
                     </div>
                 <?php endforeach ?>
             </div>
         </div>
         <div class="col-md-4 col-sm-6 d-none d-lg-block"><a class="d-block" href="shop-grid-ls.html"><img class="d-block rounded-3" src="<?= base_url('assets/img/home/banners/v-banner.jpg') ?>" alt="Promo banner"></a></div>
     </div>
 </section>