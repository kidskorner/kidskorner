<!-- Page Title-->
<div class="page-title-overlap bg-dark pt-4">
  <div class="container d-lg-flex justify-content-between py-2 py-lg-3">
    <div class="order-lg-2 mb-3 mb-lg-0 pt-lg-2">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light flex-lg-nowrap justify-content-center justify-content-lg-start">
          <li class="breadcrumb-item"><a class="text-nowrap" href="<?= base_url() ?>"><i class="ci-home"></i>Home</a></li>
          <!-- <li class="breadcrumb-item text-nowrap"><a href="#">Shop</a></li> -->
          <li class="breadcrumb-item text-nowrap active" aria-current="page"><?= str_replace('-', ' ', $this->uri->segment(1)) ?></li>
        </ol>
      </nav>
    </div>
    <div class="order-lg-1 pe-lg-4 text-center text-lg-start">
      <h1 class="h3 text-light mb-0"><?= str_replace('-', ' ', $this->uri->segment(1)) ?></h1>
    </div>
  </div>
</div>
<div class="container pb-5 mb-2 mb-md-4">
  <div class="row">
    <!-- Sidebar-->
    <aside class="col-lg-4">
      <!-- Sidebar-->
      <div class="offcanvas offcanvas-collapse bg-white w-100 rounded-3 shadow-lg py-1" id="shop-sidebar" style="max-width: 22rem;">
        <div class="offcanvas-header align-items-center shadow-sm">
          <h2 class="h5 mb-0">Filters</h2>
          <button class="btn-close ms-auto" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body py-grid-gutter px-lg-grid-gutter">

          <!-- Categories-->
          <?php $this->load->view('common/product-categories') ?>

          <!-- Filter by Size-->
          <?php $this->load->view('common/filter-size') ?>

          <!-- Filter by Color-->
          <?php $this->load->view('common/filter-colour') ?>

        </div>
      </div>
      <!-- Popular Products  -->
      <div class="widget mt-5 pe-5 d-none d-lg-block">
        <h3 class="widget-title">Best offer products</h3>
        <?php foreach ($best_offer_products as $product) : ?>
          <div class="d-flex align-items-center pb-2 border-bottom">
            <a class="d-block" href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>">
              <img src="<?= base_url('images/product/' . $product->image_1) ?>" width="64" alt="<?= $product->name ?>">
            </a>
            <div class="ps-2">
              <h6 class="widget-product-title"><a href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>"><?= $product->name ?></a></h6>
              <div class="widget-product-meta"><span class="text-accent me-2">₹ <?= $product->sale_price ?></span></div>
            </div>
          </div>
        <?php endforeach ?>
      </div>
      <!-- Popular Products  -->
      <div class="widget mt-5 pe-5 d-none d-lg-block">
        <h3 class="widget-title">New arrival products</h3>
        <?php foreach ($new_arrival_products as $product) : ?>
          <div class="d-flex align-items-center pb-2 border-bottom">
            <a class="d-block" href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>">
              <img src="<?= base_url('images/product/' . $product->image_1) ?>" width="64" alt="<?= $product->name ?>">
            </a>
            <div class="ps-2">
              <h6 class="widget-product-title"><a href="<?= base_url($product->category . '/' . $product->subcategory . '/' . $product->slug) ?>"><?= $product->name ?></a></h6>
              <div class="widget-product-meta"><span class="text-accent me-2">₹ <?= $product->sale_price ?></span></div>
            </div>
          </div>
        <?php endforeach ?>
      </div>
    </aside>
    <!-- Content  -->
    <section class="col-lg-8">

      <?php $this->load->view('common/product-sort') ?>

      <!-- Products grid-->
      <div class="row mx-n2" id="dataList">

    </section>
  </div>
</div>

<script>
  $(function() {
    searchFilter();
  })
  var CSRFToken = '<?= $this->security->get_csrf_token_name(); ?>';
  var CSRFHash = '<?= $this->security->get_csrf_hash(); ?>';

  function searchFilter(page_num) {
    page_num = page_num ? page_num : 0;
    var colour_id = getCookieValue('colour_id');
    var size_id = getCookieValue('size_id');
    var sort_by = getCookieValue('sort_by');
    var category_slug = '<?= $this->uri->segment(1) ?>';
    var subcategory_slug = '<?= $this->uri->segment(2) ?>';
    $.ajax({
      type: 'POST',
      url: '<?php echo base_url('common/ajax_product_pagination/'); ?>' + page_num,
      data: {
        [CSRFToken]: CSRFHash,
        page: page_num,
        colour_id: colour_id,
        size_id: size_id,
        sort_by: sort_by,
        category_slug: category_slug,
        subcategory_slug: subcategory_slug
      },
      beforeSend: function() {
        $('.page-loader').show();
      },
      success: function(response) {
        // var scroll = $(window).scrollTop();
        // if (scroll > 0) {
        //   $('html, body').animate({
        //     scrollTop: 0,
        //   }, 2000);
        // }

        $('.page-loader').fadeOut("slow");
        $('#dataList').html(response);

      }
    });
  }

  function sortCookie() {
    let sort_by = $('#sort_by').val();
    $.post("<?= base_url('common/sort_cookie') ?>", {
      [CSRFToken]: CSRFHash,
      'sort_by': sort_by
    }).done(function(data) {
      searchFilter();
    });
  }

  $('.colour_cookie').click(function() {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
    } else {
      $(this).addClass('active');
    }
    $.post("<?= base_url('common/colour_cookie') ?>", {
      [CSRFToken]: CSRFHash,
      'colour_id': $(this).data('colour_id')
    }).done(function(data) {
      searchFilter();
    });
  });

  $('.size_cookie').click(function() {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
    } else {
      $(this).addClass('active');
    }
    $.post("<?= base_url('common/size_cookie') ?>", {
      [CSRFToken]: CSRFHash,
      'size_id': $(this).data('size_id')
    }).done(function(data) {
      searchFilter();
    });
  });

  function getCookieValue(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
</script>