</main>
<!-- Footer-->
<footer class="footer bg-dark pt-5">
    <div class="container">
        <div class="row pb-2">
            <div class="col-md-4 col-sm-6">
                <div class="widget widget-links widget-light pb-2 mb-4">
                    <h3 class="widget-title text-light">Shop departments</h3>
                    <ul class="widget-list">
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Sneakers &amp; Athletic</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Athletic Apparel</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Sandals</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Jeans</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Shirts &amp; Tops</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Shorts</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">T-Shirts</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Swimwear</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Clogs &amp; Mules</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Bags &amp; Wallets</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Accessories</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Sunglasses &amp; Eyewear</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Watches</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="widget widget-links widget-light pb-2 mb-4">
                    <h3 class="widget-title text-light">Account &amp; shipping info</h3>
                    <ul class="widget-list">
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Your account</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Shipping rates &amp; policies</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Refunds &amp; replacements</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Order tracking</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Delivery info</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Taxes &amp; fees</a></li>
                    </ul>
                </div>
                <div class="widget widget-links widget-light pb-2 mb-4">
                    <h3 class="widget-title text-light">About us</h3>
                    <ul class="widget-list">
                        <li class="widget-list-item"><a class="widget-list-link" href="#">About company</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Our team</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">Careers</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="#">News</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="widget pb-2 mb-4">
                    <h3 class="widget-title text-light pb-1">Stay informed</h3>
                    <form class="subscription-form validate" action="https://studio.us12.list-manage.com/subscribe/post?u=c7103e2c981361a6639545bd5&amp;amp;id=29ca296126" method="post" name="mc-embedded-subscribe-form" target="_blank" novalidate>
                        <div class="input-group flex-nowrap"><i class="ci-mail position-absolute top-50 translate-middle-y text-muted fs-base ms-3"></i>
                            <input class="form-control rounded-start" type="email" name="EMAIL" placeholder="Your email" required>
                            <button class="btn btn-primary" type="submit" name="subscribe">Subscribe*</button>
                        </div>
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true">
                            <input class="subscription-form-antispam" type="text" name="b_c7103e2c981361a6639545bd5_29ca296126" tabindex="-1">
                        </div>
                        <div class="form-text text-light opacity-50">*Subscribe to our newsletter to receive early discount offers, updates and new products info.</div>
                        <div class="subscription-status"></div>
                    </form>
                </div>
                <div class="widget pb-2 mb-4">
                    <h3 class="widget-title text-light pb-1">Download our app</h3>
                    <div class="d-flex flex-wrap">
                        <div class="me-2 mb-2"><a class="btn-market btn-apple" href="#" role="button"><span class="btn-market-subtitle">Download on the</span><span class="btn-market-title">App Store</span></a></div>
                        <div class="mb-2"><a class="btn-market btn-google" href="#" role="button"><span class="btn-market-subtitle">Download on the</span><span class="btn-market-title">Google Play</span></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pt-5 bg-darker">
        <div class="container">
            <div class="row pb-3">
                <div class="col-md-3 col-sm-6 mb-4">
                    <div class="d-flex"><i class="ci-rocket text-primary" style="font-size: 2.25rem;"></i>
                        <div class="ps-3">
                            <h6 class="fs-base text-light mb-1">Fast and free delivery</h6>
                            <p class="mb-0 fs-ms text-light opacity-50">Free delivery for all orders over $200</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 mb-4">
                    <div class="d-flex"><i class="ci-currency-exchange text-primary" style="font-size: 2.25rem;"></i>
                        <div class="ps-3">
                            <h6 class="fs-base text-light mb-1">Money back guarantee</h6>
                            <p class="mb-0 fs-ms text-light opacity-50">We return money within 30 days</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 mb-4">
                    <div class="d-flex"><i class="ci-support text-primary" style="font-size: 2.25rem;"></i>
                        <div class="ps-3">
                            <h6 class="fs-base text-light mb-1">24/7 customer support</h6>
                            <p class="mb-0 fs-ms text-light opacity-50">Friendly 24/7 customer support</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 mb-4">
                    <div class="d-flex"><i class="ci-card text-primary" style="font-size: 2.25rem;"></i>
                        <div class="ps-3">
                            <h6 class="fs-base text-light mb-1">Secure online payment</h6>
                            <p class="mb-0 fs-ms text-light opacity-50">We possess SSL / Secure сertificate</p>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="hr-light mb-5">
            <div class="row pb-2">
                <div class="col-md-6 text-center text-md-start mb-4">
                    <div class="widget widget-links widget-light">
                        <ul class="widget-list d-flex flex-wrap justify-content-center justify-content-md-start">
                            <li class="widget-list-item me-4"><a class="widget-list-link" href="#">Outlets</a></li>
                            <li class="widget-list-item me-4"><a class="widget-list-link" href="#">Affiliates</a></li>
                            <li class="widget-list-item me-4"><a class="widget-list-link" href="#">Support</a></li>
                            <li class="widget-list-item me-4"><a class="widget-list-link" href="#">Privacy</a></li>
                            <li class="widget-list-item me-4"><a class="widget-list-link" href="#">Terms of use</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 text-center text-md-end mb-4">
                    <div class="mb-3">
                        <a class="btn-social bs-light bs-twitter ms-2 mb-2" href="https://twitter.com/kidskorner_in" target="_blank"><i class="ci-twitter"></i></a>
                        <a class="btn-social bs-light bs-facebook ms-2 mb-2" href="https://www.facebook.com/people/Kidskornerfashion/100089716058435/" target="_blank"><i class="ci-facebook"></i></a>
                        <a class="btn-social bs-light bs-instagram ms-2 mb-2" href="https://www.instagram.com/kidskornerfashion/" target="_blank"><i class="ci-instagram"></i></a>
                        <a class="btn-social bs-light bs-pinterest ms-2 mb-2" href="https://www.pinterest.com/kidskornerfashion/" target="_blank"><i class="ci-pinterest"></i></a>
                        <a class="btn-social bs-light bs-youtube ms-2 mb-2" href="https://www.youtube.com/@kidsfashion-in" target="_blank"><i class="ci-youtube"></i></a>
                    </div>
                    <img class="d-inline-block" src="<?= base_url('assets/img/cards-alt.png') ?>" width="187" alt="Payment methods">
                </div>
            </div>
            <div class="pb-4 fs-xs text-light opacity-50 text-center text-md-start">© All rights reserved<a class="text-light" href="#" target="_blank" rel="noopener">Kidskorner</a></div>
        </div>
    </div>
</footer>
<!-- Toolbar for handheld devices (Default)-->
<div class="handheld-toolbar">
    <div class="d-table table-layout-fixed w-100">
        <?php if ($this->controller == 'category' || $this->controller == 'subcategory') : ?>
            <a class="d-table-cell handheld-toolbar-item" href="#" data-bs-toggle="offcanvas" data-bs-target="#shop-sidebar"><span class="handheld-toolbar-icon"><i class="ci-filter-alt"></i></span><span class="handheld-toolbar-label">Filters</span></a>
        <?php endif ?>
        <a class="d-table-cell handheld-toolbar-item" href="<?= base_url('wishlist') ?>"><span class="handheld-toolbar-icon"><i class="ci-heart"></i></span><span class="handheld-toolbar-label">Wishlist</span></a>
        <a class="d-table-cell handheld-toolbar-item" href="javascript:void(0)" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" onclick="window.scrollTo(0, 0)"><span class="handheld-toolbar-icon"><i class="ci-menu"></i></span><span class="handheld-toolbar-label">Menu</span></a>
        <a class="d-table-cell handheld-toolbar-item" href="<?= base_url('cart') ?>"><span class="handheld-toolbar-icon"><i class="ci-cart"></i><span class="badge bg-primary rounded-pill ms-1">4</span></span><span class="handheld-toolbar-label">$265.00</span></a>
    </div>
</div>
<!-- Back To Top Button-->
<a class="btn-scroll-top" href="#top" data-scroll><span class="btn-scroll-top-tooltip text-muted fs-sm me-2">Top</span><i class="btn-scroll-top-icon ci-arrow-up"> </i></a>

<!-- Vendor scrits: js libraries and plugins-->
<script src="<?= base_url('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/simplebar/dist/simplebar.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/tiny-slider/dist/min/tiny-slider.js') ?>"></script>
<script src="<?= base_url('assets/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/drift-zoom/dist/Drift.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/lightgallery/lightgallery.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/lightgallery/plugins/video/lg-video.min.js') ?>"></script>
<script src="<?= base_url('assets/toaster/js/toastr.min.js?v1.1') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- Main theme script-->
<script src="<?= base_url('assets/js/theme.min.js') ?>"></script>
</body>

</html>
<script>
    $(function() {

        var CSRFToken = '<?= $this->security->get_csrf_token_name(); ?>';
        var CSRFHash = '<?= $this->security->get_csrf_hash(); ?>';

        $('.my-account').click(function() {
            let otp = "<?= $this->session->has_userdata('otp') ?>";
            if (otp) {
                $('#signin-modal').modal('hide');
                $('#otp-modal').modal('show');
            } else {
                $('#signin-modal').modal('show');
                $('#otp-modal').modal('hide');
            }
        })

        $('.wishlist').click(function() {
            let user_id = "<?= isUser() ?>";
            if (user_id) {
                window.location.href = "<?= base_url('wishlist') ?>";
            } else {
                $('#signin-modal').modal('show');
            }
        })


        $('.sign-in').click(function(e) {
            e.preventDefault();
            $.ajax({
                method: "post",
                url: "<?= base_url('login/sign_in') ?>",
                data: $('#form-signin').serialize(),
                dataType: "json",
                beforeSend: function() {
                    $('#sign-in').html(`<span class="spinner-border spinner-border-sm me-2" role="status" aria-hidden="true"></span>Loading...`).attr('disabled', 'true');
                    $('.phone').remove();
                },
                success: function(response) {

                    $('#sign-in').html(`<i class="ci-sign-in me-2 ms-n21"></i>Sign In`).removeAttr('disabled');

                    if (response.status == 'otp') {
                        $('#signin-modal').modal('hide');
                        $('#otp-modal').modal('show');
                    }

                    if (response.status.phone) {
                        $('#phone').after(response.status.phone);
                    }
                }
            })
        })


        $('.verify-otp').click(function(e) {
            e.preventDefault();

            let first = $('#first').val();
            let second = $('#second').val();
            let third = $('#third').val();
            let fourth = $('#fourth').val();

            if(first == '' || second == '' || third == '' || fourth == '')
            {
                $('#otp').after("<span class='text-danger otp'>Please enter 4 digit OTP</span>");
                return false;
            }

            $.ajax({
                method: "post",
                url: "<?= base_url('login/verify_otp') ?>",
                data: $('#form-otp').serialize(),
                dataType: "json",
                beforeSend: function() {
                    $('#verify-otp').html(`<span class="spinner-border spinner-border-sm me-2" role="status" aria-hidden="true"></span>Loading...`).attr('disabled', 'true');
                    $('.otp').remove();
                },
                success: function(response) {

                    $('#verify-otp').html(`<i class="ci-sign-in me-2 ms-n21"></i>Submit`).removeAttr('disabled');

                    if (response.status == 'match') {
                        location.reload();
                    }

                    if (response.status.otp) {
                        $('#otp').after(response.status.otp);
                    }
                }
            })
        })


        $('.resend').click(function() {
            $('#otp-message').html();
            $.ajax({
                method: "post",
                url: "<?= base_url('login/resend') ?>",
                data: {
                    [CSRFToken]: CSRFHash
                },
                dataType: "json",
                beforeSend: function() {
                    $('.resend').html(`<span class="spinner-border spinner-border-sm me-2" role="status" aria-hidden="true"></span>sending...`).attr('disabled', 'true');
                    $('.otp').remove();
                },
                success: function(response) {
                    $('.resend').html(`Recent`).removeAttr('disabled');

                    if (response.status == 'sent') {
                        $('#otp-message').html(`<div class="alert alert-success d-flex" role="alert">
                            <div class="alert-icon">
                                <i class="ci-check-circle"></i>
                            </div>
                            <div>OTP sent successfully.</div>
                        </div>`);
                    }

                    if (response.status == 'faild') {
                        $('#otp-message').html(`<div class="alert alert-danger d-flex" role="alert">
                            <div class="alert-icon">
                                <i class="ci-close-circle"></i>
                            </div>
                            <div>Faild, Please try again!</div>
                            </div>`);
                    }

                }
            })
        })


        $('.otp-back').click(function() {
            $('#otp-modal').modal('hide');
            $('#signin-modal').modal('show');
        })


        $("body").on("click", ".add-to-wishlist", function() {

            let product_id = $(this).data('product-id');
            let user_id = "<?= $this->session->userdata('user_id') ?>";

            if (user_id) {
                $.ajax({
                    method: "post",
                    url: "<?= base_url('account/add_to_wishlist') ?>",
                    data: {
                        [CSRFToken]: CSRFHash,
                        product_id: product_id
                    },
                    dataType: "json",
                    beforeSend: function() {
                        $('.pro-' + product_id).attr('disabled', 'true');
                    },
                    success: function(response) {
                        $('.pro-' + product_id).removeAttr('disabled');

                        if (response.status == 'success') {
                            $.toastr.success(response.message);
                            $('.pro-' + product_id).addClass('wishlisted');
                        }

                        if (response.status == 'info') {
                            $.toastr.info(response.message);
                            $('.pro-' + product_id).removeClass('wishlisted');
                        }

                        if (response.status == 'error') {
                            $.toastr.error(response.message);
                        }
                    }
                })
            } else {
                $('#signin-modal').modal('show');
            }
        })


        $("body").on("click", ".add-to-cart", function() {

            let product_id = $(this).data('product-id');
            let colour_id = $("input[name=colour_id]:checked").val();
            let size_id = $("input[name=size_id]:checked").val();
            let qty = $('#qty').val();
            let user_id = "<?= $this->session->userdata('user_id') ?>";

            if (size_id == undefined) {
                $.toastr.warning("Please select size");
                return false;
            }

            if (user_id) {
                $.ajax({
                    method: "post",
                    url: "<?= base_url('account/add_to_cart') ?>",
                    data: {
                        [CSRFToken]: CSRFHash,
                        product_id: product_id,
                        colour_id: colour_id,
                        size_id: size_id,
                        qty: qty
                    },
                    dataType: "json",
                    beforeSend: function() {
                        $('.add-to-cart').attr('disabled', 'true');
                    },
                    success: function(response) {
                        $('.add-to-cart').removeAttr('disabled');

                        if (response.status == 'success') {
                            $('#cart-count').text(response.cart_count);
                            $.toastr.success(response.message);
                        }

                        if (response.status == 'info') {
                            $('#cart-count').text(response.cart_count);
                            $.toastr.info(response.message);
                        }

                        if (response.status == 'warning') {
                            $.toastr.warning(response.message);
                        }

                        if (response.status == 'error') {
                            $.toastr.error(response.message);
                        }
                    }
                })
            } else {
                $('#signin-modal').modal('show');
            }
        })



    })

    function OTPInput() {
        const inputs = document.querySelectorAll('#otp > *[id]');
        for (let i = 0; i < inputs.length; i++) {
            inputs[i].addEventListener('keydown', function(event) {
                if (event.key === "Backspace") {
                    inputs[i].value = '';
                    if (i !== 0) inputs[i - 1].focus();
                } else {
                    if (i === inputs.length - 1 && inputs[i].value !== '') {
                        return true;
                    } else if (event.keyCode > 47 && event.keyCode < 58) {
                        inputs[i].value = event.key;
                        if (i !== inputs.length - 1) inputs[i + 1].focus();
                        event.preventDefault();
                    } else if (event.keyCode > 64 && event.keyCode < 91) {
                        inputs[i].value = String.fromCharCode(event.keyCode);
                        if (i !== inputs.length - 1) inputs[i + 1].focus();
                        event.preventDefault();
                    }
                }
            });
        }
    }
    OTPInput();
</script>