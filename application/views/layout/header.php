<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="utf-8">
    <title>KidsKorner</title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="Kidskorner - kids fashion apparels">
    <meta name="keywords" content="">
    <meta name="author" content="Kidskorner Jithin">
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('assets/img/favicon-32x32.ico') ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/img/favicon-16x16.ico') ?>">
    <link rel="manifest" href="site.webmanifest">
    <link rel="mask-icon" color="#fe6a6a" href="safari-pinned-tab.svg">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <!-- Vendor Styles including: Font Icons, Plugins, etc.-->
    <link rel="stylesheet" media="screen" href="<?= base_url('assets/vendor/simplebar/dist/simplebar.min.css') ?>" />
    <link rel="stylesheet" media="screen" href="<?= base_url('assets/vendor/tiny-slider/dist/tiny-slider.css') ?>" />
    <link rel="stylesheet" media="screen" href="<?= base_url('assets/vendor/drift-zoom/dist/drift-basic.min.css') ?>" />
    <link rel="stylesheet" media="screen" href="<?= base_url('assets/vendor/lightgallery/css/lightgallery-bundle.min.css') ?>" />
    <link rel="stylesheet" media="screen" href="<?= base_url('assets/toaster/css/toastr.min.css?v1.1') ?>">

    <!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" href="<?= base_url('assets/css/theme.min.css') ?>">
    <script src="<?= base_url('assets/js/jquery.js') ?>"></script>

</head>
<!-- Body-->

<body class="handheld-toolbar-enabled">

    <!-- Sign in / sign up modal-->
    <div class="modal fade" id="signin-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-secondary">
                    <ul class="nav nav-tabs card-header-tabs" role="tablist">
                        <li class="nav-item"><a class="nav-link fw-medium active" href="#signin-tab" data-bs-toggle="tab" role="tab" aria-selected="true"><i class="ci-unlocked me-2 mt-n1"></i>Sign in</a></li>
                    </ul>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body tab-content py-4">
                    <form class="needs-validation tab-pane fade show active" autocomplete="off" id="form-signin">
                        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>">
                        <div class="form-floating mb-3">
                            <input class="form-control" type="text" id="phone" name="phone" placeholder="Phone Number">
                            <label for="fl-text">Enter your phone number1</label>
                        </div>
                        <div class="row justify-content-center">
                            <button class="btn btn-primary btn-shadow d-block w-25 sign-in" id="sign-in" type="submit">Sign in</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- OTP / OTP modal-->
    <div class="modal fade" id="otp-modal" tabindex="-2" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-secondary"> 
                    <ul class="nav nav-tabs card-header-tabs" role="tablist">
                        <li class="nav-item"><a class="nav-link fw-medium active" href="#otp-tab" data-bs-toggle="tab" role="tab" aria-selected="true"><i class="ci-mobile me-2 mt-n1"></i>OTP</a></li>
                    </ul>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body tab-content py-4">
                    <form class="needs-validation tab-pane fade show active" autocomplete="off" id="form-otp">
                        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>">
                        <h6 class="text-center">Please enter the one time password <br> to verify your account</h6>
                        <div id="otp" class="inputs d-flex flex-row justify-content-center mt-2">
                            <input class="m-2 text-center form-control rounded" type="text" id="first" name="first" maxlength="1" />
                            <input class="m-2 text-center form-control rounded" type="text" id="second" name="second" maxlength="1" />
                            <input class="m-2 text-center form-control rounded" type="text" id="third" name="third" maxlength="1" />
                            <input class="m-2 text-center form-control rounded" type="text" id="fourth" name="fourth" maxlength="1" />
                        </div>
                    
                        <div id="otp-message"></div>
                        <div class="row justify-content-center">
                            <button class="btn btn-primary btn-shadow w-25 verify-otp" id="verify-otp" type="submit">Validate</button>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <a href="#" class="text-decoration-none">Resend</a>
                            </div>
                            <div class="col-md-6">
                                <a class="text-decoration-none otp-back float-end" href="javascript:void(0);">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <main class="page-wrapper">
        <!-- Navbar 3 Level (Light)-->
        <header class="shadow-sm">
            <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
            <div class="navbar-sticky bg-light">
                <div class="navbar navbar-expand-lg navbar-light">
                    <div class="container">
                        <a class="navbar-brand d-none d-sm-block flex-shrink-0" href="<?= base_url() ?>">
                            <img src="<?= base_url('assets/img/logo.png') ?>" width="142" alt="KidsKorner">
                        </a>
                        <a class="navbar-brand d-sm-none flex-shrink-0 me-2" href="<?= base_url() ?>">
                            <img src="<?= base_url('assets/img/logo.png') ?>" width="170" alt="KidsKorner">
                        </a>
                        <div class="input-group d-none d-lg-flex mx-4">
                            <input class="form-control rounded-end pe-5" type="text" placeholder="Search for products"><i class="ci-search position-absolute top-50 end-0 translate-middle-y text-muted fs-base me-3"></i>
                        </div>
                        <div class="navbar-toolbar d-flex flex-shrink-0 align-items-center">

                            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse"><span class="navbar-toggler-icon"></span></button>

                            <a class="navbar-tool navbar-stuck-toggler" href="#"><span class="navbar-tool-tooltip">Expand menu</span>
                                <div class="navbar-tool-icon-box"><i class="navbar-tool-icon ci-menu"></i></div>
                            </a>

                            <a class="navbar-tool d-none d-lg-flex wishlist" href="javascript:void(0);"><span class="navbar-tool-tooltip">Wishlist</span>
                                <div class="navbar-tool-icon-box"><i class="navbar-tool-icon ci-heart"></i></div>
                            </a>

                            <?php if ($this->session->userdata('user_id')) : ?>

                                <a class="navbar-tool ms-1 ms-lg-0 me-n1 me-lg-2" href="<?= base_url('profile') ?>">
                                    <div class="navbar-tool-icon-box"><i class="navbar-tool-icon ci-user"></i></div>
                                    <div class="navbar-tool-text ms-n3"><small>Hello, <?= $this->session->has_userdata('user_name') ?></small>My Account</div>
                                </a>

                            <?php else : ?>

                                <a class="navbar-tool ms-1 ms-lg-0 me-n1 me-lg-2 my-account" href="javascript:void(0);">
                                    <div class="navbar-tool-icon-box"><i class="navbar-tool-icon ci-user"></i></div>
                                    <div class="navbar-tool-text ms-n3"><small>Hello, Sign in</small>My Account</div>
                                </a>

                            <?php endif ?>

                            <div class="navbar-tool dropdown ms-3">
                                <a class="navbar-tool-icon-box bg-secondary dropdown-toggle" href="<?= base_url('cart') ?>">
                                    <span class="navbar-tool-label" id="cart-count"><?= cartCount() ?></span>
                                    <i class="navbar-tool-icon ci-cart"></i>
                                </a>
                                <a class="navbar-tool-text" href="<?= base_url('cart') ?>"><small>My Cart</small>₹<span id="cart-amount"><?= cartAmount() ?></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="navbar navbar-expand-lg navbar-light navbar-stuck-menu mt-n2 pt-0 pb-2">
                    <div class="container">
                        <div class="collapse navbar-collapse justify-content-center" id="navbarCollapse">
                            <!-- Search-->
                            <div class="input-group d-lg-none my-3"><i class="ci-search position-absolute top-50 start-0 translate-middle-y text-muted fs-base ms-3"></i>
                                <input class="form-control rounded-start" type="text" placeholder="Search for products">
                            </div>

                            <!-- Primary menu-->
                            <ul class="navbar-nav">
                                <li class="nav-item <?= $this->uri->segment(1) == 'boys' ? 'active' : '' ?>"><a class="nav-link" href="<?= base_url('boys') ?>">For Boys</a></li>
                                <li class="nav-item <?= $this->uri->segment(1) == 'girls' ? 'active' : '' ?>"><a class="nav-link" href="<?= base_url('girls') ?>">For Girls</a></li>
                                <li class="nav-item <?= $this->uri->segment(1) == 'kids' ? 'active' : '' ?>"><a class="nav-link" href="<?= base_url('kids') ?>">For Babies</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="spinner-grow text-danger page-loader" style="display: none; width: 5rem; height: 5rem; color: var(--cz-primary); position: absolute; left: 50%; top: 50%; z-index: 100;" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>
        <style>
            .wishlisted {
                color: white;
                background: #fd6869;
            }
        </style>