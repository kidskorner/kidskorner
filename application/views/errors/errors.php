<div class="container py-2 mb-lg-3">
    <div class="row justify-content-center pt-lg-4 text-center">
        <div class="col-lg-5 col-md-7 col-sm-9">
            <!-- <h1 class="h3">404 error</h1> -->
            <h3 class="h5 fw-normal mb-2">We can't seem to find the page you are looking for.</h3>
            <img class="d-block mx-auto mb-3" src="<?= base_url('assets/img/pages/404.png') ?>" width="340" alt="404 Error">
            <p class="fs-md mb-4">
                <a href="<?= base_url() ?>" class="btn btn-primary">
                    <i class="ci-home me-2"></i>
                    Home
                </a>
            </p>
        </div>
    </div>
</div>