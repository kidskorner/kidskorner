<div class="d-flex justify-content-center justify-content-sm-between align-items-center pt-2 pb-4 pb-sm-5">
    <div class="d-flex flex-wrap">
        <div class="d-flex align-items-center flex-nowrap me-3 me-sm-4 pb-3">
            <label class="text-light opacity-75 text-nowrap fs-sm me-2 d-none d-sm-block" for="sorting">Sort by:</label>
            <select class="form-select" id="sort_by" onchange="sortCookie()">
                <option value="" <?= get_cookie('sort_by') == '' ? 'selected' : '' ?>>Default sorting</option>
                <option value="low-to-high" <?= get_cookie('sort_by') == 'low-to-high' ? 'selected' : '' ?>>Price: low to high</option>
                <option value="high-to-low" <?= get_cookie('sort_by') == 'high-to-low' ? 'selected' : '' ?>>Price: high to low</option>
                <!-- <option value="popularity">Sort by popularity</option> -->
                <option value="new" <?= get_cookie('sort_by') == 'new' ? 'selected' : '' ?>>Newness</option>
            </select>
        </div>
    </div>
    <!-- <div class="d-flex pb-3"><a class="nav-link-style nav-link-light me-3" href="#"><i class="ci-arrow-left"></i></a><span class="fs-md text-light">1 / 5</span><a class="nav-link-style nav-link-light ms-3" href="#"><i class="ci-arrow-right"></i></a></div> -->
    <!-- <div class="d-none d-sm-flex pb-3"><a class="btn btn-icon nav-link-style bg-light text-dark disabled opacity-100 me-2" href="#"><i class="ci-view-grid"></i></a><a class="btn btn-icon nav-link-style nav-link-light" href="shop-list-ls.html"><i class="ci-view-list"></i></a></div> -->
</div>