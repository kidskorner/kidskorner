<div class="widget widget-filter mb-4 pb-4 border-bottom">
    <h3 class="widget-title">Size</h3>
    <div class="input-group input-group-sm mb-2">
        <input class="widget-filter-search form-control rounded-end pe-5" type="text" placeholder="Search"><i class="ci-search position-absolute top-50 end-0 translate-middle-y fs-sm me-3"></i>
    </div>
    <ul class="widget-list widget-filter-list list-unstyled pt-1" style="max-height: 11rem;" data-simplebar data-simplebar-auto-hide="false">
        <?php $size_ids = get_cookie('size_id'); ?>
        <?php $size_array = explode(',', $size_ids); ?>
        <?php foreach ($sizes as $size) : ?>
            <li class="widget-filter-item d-flex justify-content-between align-items-center mb-1">
                <div class="form-check">
                    <input class="form-check-input size_cookie" type="checkbox" <?= in_array($size->id, $size_array) ? 'checked' : ''  ?> data-size_id="<?= $size->id ?>">
                    <label class="form-check-label widget-filter-item-text" for="size-xs"><?= $size->name ?></label>
                </div>
                <!-- <span class="fs-xs text-muted">34</span> -->
            </li>
        <?php endforeach ?>
    </ul>
</div>