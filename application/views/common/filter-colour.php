<div class="widget">
    <h3 class="widget-title">Color</h3>
    <div class="d-flex flex-wrap">
        <?php $colour_ids = get_cookie('colour_id'); ?>
        <?php $colour_array = explode(',', $colour_ids); ?>
        <?php foreach ($colours as $colour) : ?>
            <div class="form-check form-option text-center mb-2 mx-1" style="width: 4rem;">
                <input class="form-check-input colour_cookie" type="checkbox" id="<?= $colour->slug ?>" <?= in_array($colour->id, $colour_array) ? 'checked' : '' ?> data-colour_id="<?= $colour->id ?>">
                <label class="form-option-label rounded-circle" for="<?= $colour->slug ?>"><span class="form-option-color rounded-circle" style="background-color: #b3c8db;"></span></label>
                <label class="d-block fs-xs text-muted mt-n1" for="<?= $colour->slug ?>"><?= $colour->name ?></label>
            </div>
        <?php endforeach ?>
    </div>
</div>