		<?php if ($products) : ?>
			<?php foreach ($products as $product) : ?>
				<div class="col-md-4 col-sm-6 px-2 mb-4">
					<div class="card product-card shadow-sm">
						<button class="btn-wishlist btn-sm pro-<?= $product->id ?> add-to-wishlist <?= checkWishlist($product->id, $this->session->userdata('user_id')) ? 'wishlisted' : '' ?>" data-product-id="<?= $product->id ?>" type="button" data-bs-toggle="tooltip" data-bs-placement="left" title="Wishlist"><i class="ci-heart"></i></button>
						<a class="card-img-top d-block overflow-hidden" href="<?= base_url($category_slug.'/'.subcategory_slug($product->subcategory_id).'/'.$product->slug) ?>">
							<img src="<?= base_url('images/product/'.$product->image_1) ?>" alt="<?= $product->name ?>">
						</a>
						<div class="card-body py-2"><a class="product-meta d-block fs-xs pb-1" href="<?= base_url($category_slug.'/'.subcategory_slug($product->subcategory_id)) ?>"><?= str_replace('-', ' ', subcategory_slug($product->subcategory_id)) ?></a>
							<h3 class="product-title fs-sm"><a href="<?= base_url($category_slug.'/'.subcategory_slug($product->subcategory_id).'/'.$product->slug) ?>"><?= $product->name ?></a></h3>
							<div class="d-flex justify-content-between">
								<div class="product-price"><span class="text-accent">₹ <?= $product->sale_price ?></span></div>
								<!-- <div class="star-rating"><i class="star-rating-icon ci-star-filled active"></i><i class="star-rating-icon ci-star-filled active"></i><i class="star-rating-icon ci-star-filled active"></i><i class="star-rating-icon ci-star-filled active"></i><i class="star-rating-icon ci-star"></i></div> -->
							</div>
						</div>
						<div class="card-body">
							<button class="btn btn-primary btn-sm d-block w-100 mb-2" type="button"><i class="ci-cart fs-sm me-1"></i>Add to Cart</button>
						</div>
					</div>
					<hr class="d-sm-none">
				</div>
			<?php endforeach ?>
		<?php else : ?>
			<div class="col-md-12 col-sm-12 px-2 mb-4">
				<div class="alert alert-danger" role="alert" style="margin-top: 20px;"><strong>Sorry! </strong> Products not found...</div>
				<img class="d-block mx-auto mb-3" src="<?= base_url('assets/img/pages/not-found.png') ?>" width="340" alt="404 Error">
			</div>
		<?php endif ?>

		</div>
		<hr class="my-3">

		<?php echo $this->ajax_pagination->create_links(); ?>