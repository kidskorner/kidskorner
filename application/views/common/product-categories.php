<div class="widget widget-categories mb-4 pb-4 border-bottom">
    <h3 class="widget-title">Categories</h3>
    <div class="accordion mt-n1" id="shop-categories">
        <?php foreach ($categories as $category) : ?>
            <div class="accordion-item">
                <h3 class="accordion-header"><a class="accordion-button <?= $this->uri->segment(1) == $category->slug ? '' : 'collapsed' ?>" href="#<?= $category->slug ?>" role="button" data-bs-toggle="collapse" aria-expanded="true" aria-controls="<?= $category->name ?>"><?= $category->name ?></a></h3>
                <div class="<?= $this->uri->segment(1) == $category->slug ? 'accordion-collapse collapse show' : 'collapse' ?> " id="<?= $category->slug ?>" data-bs-parent="#shop-categories">
                    <div class="accordion-body">
                        <div class="widget widget-links widget-filter">
                            <div class="input-group input-group-sm mb-2">
                                <input class="widget-filter-search form-control rounded-end" type="text" placeholder="Search"><i class="ci-search position-absolute top-50 end-0 translate-middle-y fs-sm me-3"></i>
                            </div>
                            <ul class="widget-list widget-filter-list pt-1" style="height: 12rem;" data-simplebar data-simplebar-auto-hide="false">
                                <?php foreach (subcategories($category->id) as $subcategory) : ?>
                                    <li class="widget-list-item widget-filter-item <?= $this->uri->segment(2) == $subcategory->slug ? 'active' : '' ?>"><a class="widget-list-link d-flex justify-content-between align-items-center" href="<?= base_url($category->slug . '/' . $subcategory->slug) ?>"><span class="widget-filter-item-text"><?= $subcategory->name ?></span><!--<span class="fs-xs text-muted ms-3">235</span>--></a></li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>