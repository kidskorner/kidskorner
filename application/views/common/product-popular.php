<div class="widget mercado-widget widget-product">
    <h2 class="widget-title">Popular Products</h2>
    <div class="widget-content">
        <ul class="products">
            <?php foreach ($popular_products as $product) : ?>
                <li class="product-item">
                    <div class="product product-widget-style">
                        <div class="thumbnnail">
                            <a href="detail.html" title="<?= $product->name ?>">
                                <figure><img src="<?= base_url('admin/uploads/product/' . $product->image_1) ?>" alt="<?= $product->name ?>"></figure>
                            </a>
                        </div>
                        <div class="product-info">
                            <a href="#" class="product-name"><span><?= $product->name ?></span></a>
                            <div class="wrap-price"><span class="product-price">₹ <?= $product->regular_price ?></span></div>
                        </div>
                    </div>
                </li>
            <?php endforeach ?>
        </ul>
    </div>
</div>