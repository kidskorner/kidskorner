<!-- Page Title-->
<div class="page-title-overlap bg-dark pt-4">
  <div class="container d-lg-flex justify-content-between py-2 py-lg-3">
    <div class="order-lg-2 mb-3 mb-lg-0 pt-lg-2">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light flex-lg-nowrap justify-content-center justify-content-lg-start">
          <li class="breadcrumb-item"><a class="text-nowrap" href="<?= base_url() ?>"><i class="ci-home"></i>Home</a></li>
          <li class="breadcrumb-item text-nowrap active" aria-current="page">Wishlist</li>
        </ol>
      </nav>
    </div>
    <div class="order-lg-1 pe-lg-4 text-center text-lg-start">
      <h1 class="h3 text-light mb-0">My wishlist</h1>
    </div>
  </div>
</div>
<div class="container pb-5 mb-2 mb-md-4">
  <div class="row">
    <!-- Sidebar-->
    <?php $this->load->view('account/navigation') ?>
    <!-- Content  -->
    <section class="col-lg-8">
      <!-- Toolbar-->
      <div class="d-none d-lg-flex justify-content-between align-items-center pt-lg-3 pb-4 pb-lg-5 mb-lg-3">
        <h6 class="fs-base text-light mb-0">List of items you added to wishlist:</h6><a class="btn btn-primary btn-sm" href="<?= base_url('login/logout')?>"><i class="ci-sign-out me-2"></i>Sign out</a>
      </div>
      <!-- Wishlist-->
      <!-- Item-->
      <?php if ($wishlists) : ?>
        <?php foreach ($wishlists as $wishlist) : ?>
          <div class="d-sm-flex justify-content-between mt-lg-4 mb-4 pb-3 pb-sm-2 border-bottom" id="w-<?= $wishlist->id ?>">
            <div class="d-block d-sm-flex align-items-start text-center text-sm-start"><a class="d-block flex-shrink-0 mx-auto me-sm-4" href="<?= base_url($wishlist->category . '/' . $wishlist->subcategory . '/' . $wishlist->slug) ?>" style="width: 10rem;">
                <img src="<?= base_url('images/product/' . $wishlist->image_1) ?>" alt="<?= $wishlist->name ?>"></a>
              <div class="pt-2">
                <h3 class="product-title fs-base mb-2"><a href="shop-single-v1.html"><?= $wishlist->name ?></a></h3>
                <div class="fs-lg text-accent pt-2">₹ <?= $wishlist->sale_price ?></div>
              </div>
            </div>
            <div class="pt-2 ps-sm-3 mx-auto mx-sm-0 text-center">
              <button class="btn btn-outline-danger btn-sm remove-wishlist" data-wishlist-id="<?= $wishlist->id ?>" type="button"><i class="ci-trash me-2"></i>Remove</button>
            </div>
          </div>
        <?php endforeach ?>
      <?php else : ?>
        <div class="alert alert-danger d-flex" role="alert">
          <div class="alert-icon">
            <i class="ci-security-announcement"></i>
          </div>
          <div>Your wishlist is empty.</div>
        </div>
        <img class="d-block mx-auto mb-3" src="<?= base_url('assets/img/pages/not-found.png') ?>" width="340" alt="Empty">
      <?php endif ?>
    </section>
  </div>
</div>

<script>
  $(function() {

    var CSRFToken = '<?= $this->security->get_csrf_token_name(); ?>';
    var CSRFHash = '<?= $this->security->get_csrf_hash(); ?>';

    $('.remove-wishlist').click(function() {
      let wishlist_id = $(this).data('wishlist-id');
      Swal.fire({
        title: 'Are you sure?',
        text: "Do you want to remove this item from your wishlist",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            method: "post",
            url: "<?= base_url('account/remove_from_wishlist') ?>",
            data: {
              [CSRFToken]: CSRFHash,
              wishlist_id: wishlist_id
            },
            dataType: "json",
            success: function(response) {
              $('#wishlist_count').text(response.wishlist_count);
              if (response.status == 'success') {                
                $.toastr.success(response.message);
                $('#w-' + wishlist_id).remove();
              }

              if (response.status == 'error') {
                $.toastr.error(response.message);
              }
            }
          });
        }
      });
    });


  })
</script>