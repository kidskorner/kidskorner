<!-- Page Title-->
<div class="page-title-overlap bg-dark pt-4">
        <div class="container d-lg-flex justify-content-between py-2 py-lg-3">
          <div class="order-lg-2 mb-3 mb-lg-0 pt-lg-2">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb breadcrumb-light flex-lg-nowrap justify-content-center justify-content-lg-start">
                <li class="breadcrumb-item"><a class="text-nowrap" href="<?= base_url() ?>"><i class="ci-home"></i>Home</a></li>                
                <li class="breadcrumb-item text-nowrap active" aria-current="page">Cart</li>
              </ol>
            </nav>
          </div>
          <div class="order-lg-1 pe-lg-4 text-center text-lg-start">
            <h1 class="h3 text-light mb-0">Your cart</h1>
          </div>
        </div>
      </div>
      <div class="container pb-5 mb-2 mb-md-4">
        <div class="row">
          <!-- List of items-->
          <section class="col-lg-8">
            <div class="d-flex justify-content-between align-items-center pt-3 pb-4 pb-sm-5 mt-1">
              <h2 class="h6 text-light mb-0">Products</h2><a class="btn btn-outline-primary btn-sm ps-2" href="<?= base_url() ?>"><i class="ci-arrow-left me-2"></i>Continue shopping</a>
            </div>
            <!-- Item-->
            <?php if($carts): ?>
            <?php foreach($carts as $cart): ?>
            <div class="d-sm-flex justify-content-between align-items-center my-2 pb-3 border-bottom" id="c-<?= $cart->id ?>">
              <div class="d-block d-sm-flex align-items-center text-center text-sm-start">
                <a class="d-inline-block flex-shrink-0 mx-auto me-sm-4" href="shop-single-v1.html">
                  <img src="<?= base_url('images/product/'.$cart->image_1) ?>" width="160" alt="Product">
                </a>
                <div class="pt-2">
                  <h3 class="product-title fs-base mb-2"><a href="shop-single-v1.html"><?= $cart->name ?></a></h3>
                  <div class="fs-sm"><span class="text-muted me-2">Size:</span><?= $cart->size ?></div>
                  <div class="fs-sm"><span class="text-muted me-2">Color:</span><?= $cart->colour ?></div>
                  <div class="fs-lg text-accent pt-2">₹ <?= $cart->sale_price ?></div>
                </div>
              </div>
              <div class="pt-2 pt-sm-0 ps-sm-3 mx-auto mx-sm-0 text-center text-sm-start" style="max-width: 9rem;">
                <label class="form-label" for="quantity1">Quantity</label>
                <input class="form-control" type="number" id="quantity1" min="1" value="<?= $cart->qty ?>">
                <button class="btn btn-link px-0 text-danger remove-cart" data-cart-id="<?= $cart->id ?>" type="button"><i class="ci-close-circle me-2"></i><span class="fs-sm">Remove</span></button>
              </div>
            </div>
            <?php endforeach ?>
                   <?php else: ?>
                    <div class="alert alert-danger d-flex mt-3" role="alert">
          <div class="alert-icon">
            <i class="ci-security-announcement"></i>
          </div>
          <div>Your cart is empty.</div>
        </div>
        <img class="d-block mx-auto mb-3" src="<?= base_url('assets/img/pages/not-found.png') ?>" width="340" alt="Empty">
        <?php endif ?>    
                       
          </section>
          <!-- Sidebar-->
          <aside class="col-lg-4 pt-4 pt-lg-0 ps-xl-5">
            <div class="bg-white rounded-3 shadow-lg p-4">
              <div class="py-2 px-xl-2">
                <div class="text-center mb-4 pb-3 border-bottom">
                  <h2 class="h6 mb-3 pb-1">Subtotal</h2>
                  <h3 class="fw-normal">₹ <span class="cart-amount"><?= cartAmount() ?></span></h3>
                </div>                
                <div class="accordion" id="order-options">
                  <div class="accordion-item">
                    <h3 class="accordion-header"><a class="accordion-button" href="#promo-code" role="button" data-bs-toggle="collapse" aria-expanded="true" aria-controls="promo-code">Apply promo code</a></h3>
                    <div class="accordion-collapse collapse show" id="promo-code" data-bs-parent="#order-options">
                      <form class="accordion-body needs-validation" method="post" novalidate>
                        <div class="mb-3">
                          <input class="form-control" type="text" placeholder="Promo code" required>
                          <div class="invalid-feedback">Please provide promo code.</div>
                        </div>
                        <button class="btn btn-outline-primary d-block w-100" type="submit">Apply promo code</button>
                      </form>
                    </div>
                  </div>                 
                </div><a class="btn btn-primary btn-shadow d-block w-100 mt-4" href="<?= base_url('checkout')?>"><i class="ci-card fs-lg me-2"></i>Proceed to Checkout</a>
              </div>
            </div>
          </aside>
        </div>
      </div>

      <script>
  $(function() {

    var CSRFToken = '<?= $this->security->get_csrf_token_name(); ?>';
    var CSRFHash = '<?= $this->security->get_csrf_hash(); ?>';

    $('.remove-cart').click(function() {
      let cart_id = $(this).data('cart-id');
      Swal.fire({
        title: 'Are you sure?',
        text: "Do you want to remove this item from your cart",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            method: "post",
            url: "<?= base_url('account/remove_from_cart') ?>",
            data: {
              [CSRFToken]: CSRFHash,
              cart_id: cart_id
            },
            dataType: "json",
            success: function(response) {
              $('#cart-count').text(response.cart_count);
              $('#cart-amount').text(response.cart_amount);
              $('.cart-amount').text(response.cart_amount);
              
              if (response.status == 'success') {                
                $.toastr.success(response.message);
                $('#c-' + cart_id).remove();
              }

              if (response.status == 'error') {
                $.toastr.error(response.message);
              }
            }
          });
        }
      });
    });


  })
</script>