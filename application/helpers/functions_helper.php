<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
function d($array = array())
{
    echo '<pre>';
    print_r($array);
    die;
}

function isAdmin() {
    $CI = & get_instance();
    $roleId = $CI->session->userdata('role_id');
    if (isset($roleId) && $roleId == 1) {
        return true;
    }
    return false;
}

function isUser() {
    $CI = & get_instance();
    $roleId = $CI->session->userdata('user_id');
    if (isset($roleId)) {
        return true;
    }
    return false;
}

function checkWishlist($product_id, $user_id) {
    $CI = & get_instance();
    $CI->load->model('User_model', 'user');
    $count = $CI->user->exist_wishlist($product_id, $user_id);
    if ($count>0) {
        return true;
    }
    return false;
}

function sendOTP($phone, $otp)
{
    $user = 'success'; 
    $pass = 'Sms@1234';
    $sender = 'BHAINF';
    $text =  "Dear User, your OTP is : $otp, Thank you for using our service. BhashSMS";
    $message = urlencode($text);
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://bhashsms.com/api/sendmsg.php?user=$user&pass=$pass&sender=$sender&phone=$phone&text=$message&priority=ndnd&stype=normal",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,        
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    
	return $response;
}

function subcategory_slug($subcategory_id)
{
    $ci = &get_instance();
    $ci->load->model('Home_model', 'home');
    return $ci->home->subcategoryslug_by_subcategoryid($subcategory_id);
}

function subcategories($category_id)
{
    $ci = &get_instance();
    $ci->load->model('Home_model', 'home');
    $result = $ci->home->subcategories($category_id);
    return $result;
}

function cartCount()
{
    $CI = & get_instance();
    $user_id = $CI->session->userdata('user_id');

    $ci = &get_instance();
    $ci->load->model('User_model', 'user');
    return $ci->user->cart_count($user_id);
}

function cartAmount()
{
    $CI = & get_instance();
    $user_id = $CI->session->userdata('user_id');

    $ci = &get_instance();
    $ci->load->model('User_model', 'user');
    return $ci->user->cart_amount($user_id);
}

function filter_url($filter)
{
    $ci = &get_instance();
    $array_filter = explode('=', $filter);
    $current_url = current_url();

    $colour = $ci->input->get('colour');
    $size = $ci->input->get('size');
    $sort = $ci->input->get('sort');

    // Colour filter
    if ($array_filter[0] == 'colour') {
        if ($colour) {
            $colours = explode(',', $colour);
            if (in_array($array_filter[1], $colours)) {
                $arr = array_diff($colours, array($array_filter[1]));
                if (empty($arr)) {
                    $colour_array = [];
                } else {
                    $colour_array = ["colour=" . implode(',', $arr)];
                }
            } else {
                $colour_array = ["colour=$colour,$array_filter[1]"];
            }
        } else {
            $colour_array = ["colour=$array_filter[1]"];
        }
    } else {
        if ($colour) {
            $colour_array = ["colour=$colour"];
        } else {
            $colour_array = [];
        }
    }

    // Size filter
    if ($array_filter[0] == 'size') {
        if ($size) {
            $sizes = explode(',', $size);
            if (in_array($array_filter[1], $sizes)) {
                $arr = array_diff($sizes, array($array_filter[1]));
                if (empty($arr)) {
                    $size_array = [];
                } else {
                    $size_array = ["size=" . implode(',', $arr)];
                }
            } else {
                $size_array = ["size=$size,$array_filter[1]"];
            }
        } else {
            $size_array = ["size=$array_filter[1]"];
        }
    } else {
        if ($size) {
            $size_array = ["size=$size"];
        } else {
            $size_array = [];
        }
    }

    // Sort
    if ($array_filter[0] == 'sort') {  
        if($array_filter[1]){
            $sort_array = ["sort=$array_filter[1]"];  
        } else{
            $sort_array = [];  
        }                         
    } else {     
        if($sort){
            $sort_array = ["sort=$sort"]; 
        }else{
            $sort_array = [];
        }        
    }


    $query_string = $_SERVER['QUERY_STRING'];
    if(empty($colour_array) && empty($size_array) && empty($sort_array)){
        return $current_url;
    }else{
        if ($query_string) {
            // return $current_url . '?' . implode('&', array_merge($colour_array, $size_array, $sort_array));
            return  '?' . implode('&', array_merge($colour_array, $size_array, $sort_array));
        } else {
            
            // return $current_url . '?' . $filter;
            return '?' . $filter;
        }
    }
    
}
