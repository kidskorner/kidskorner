<?php
class Home_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();        
    }

    public function banners()
    {        
        $this->db->where('is_active', 1);
        $this->db->order_by('id', 'DESC');        
        $query = $this->db->get('banners');
        return $query->result();
    }

    public function categories()
    {
        $this->db->select('id, name, slug');
        $this->db->where('is_active', 1);
        $query = $this->db->get('categories');
        return $query->result();
    }

    public function trending_products()
    {
        $this->db->select('c.slug as category, s.slug as subcategory, p.*');
        $this->db->from('products p');
        $this->db->join('categories c', 'c.id = p.category_id');
        $this->db->join('subcategories s', 's.id = p.subcategory_id');
        $this->db->order_by('p.id', 'DESC');
        $this->db->limit(12);
        $query = $this->db->get();
        return $query->result();
    }

    public function boy_products()
    {
        $this->db->select('c.slug as category, s.slug as subcategory, p.*');
        $this->db->from('products p');
        $this->db->join('categories c', 'c.id = p.category_id');
        $this->db->join('subcategories s', 's.id = p.subcategory_id');
        $this->db->where('p.category_id', 1);
        $this->db->order_by('p.id', 'DESC');
        $this->db->limit(12);
        $query = $this->db->get();
        return $query->result();
    }

    public function girl_products()
    {
        $this->db->select('c.slug as category, s.slug as subcategory, p.*');
        $this->db->from('products p');
        $this->db->join('categories c', 'c.id = p.category_id');
        $this->db->join('subcategories s', 's.id = p.subcategory_id');
        $this->db->where('p.category_id', 2);
        $this->db->order_by('p.id', 'DESC');
        $this->db->limit(12);
        $query = $this->db->get();
        return $query->result();
    }

    public function best_offer_products()
    {
        $this->db->select('c.slug as category, s.slug as subcategory, p.*');
        $this->db->from('products p');
        $this->db->join('categories c', 'c.id = p.category_id');
        $this->db->join('subcategories s', 's.id = p.subcategory_id');
        $this->db->order_by('p.sale_price', 'asc');
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->result();
    }

    public function new_arrival_products()
    {
        $this->db->select('c.slug as category, s.slug as subcategory, p.*');
        $this->db->from('products p');
        $this->db->join('categories c', 'c.id = p.category_id');
        $this->db->join('subcategories s', 's.id = p.subcategory_id');
        $this->db->order_by('p.sale_price', 'desc');
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->result();
    }

    public function related_products($category_id, $subcategory_id, $product_id)
    {
        $this->db->select('c.slug as category, s.slug as subcategory, p.*');
        $this->db->from('products p');
        $this->db->join('categories c', 'c.id = p.category_id');
        $this->db->join('subcategories s', 's.id = p.subcategory_id');
        $this->db->where('p.category_id', $category_id);
        $this->db->where('p.subcategory_id', $subcategory_id);
        $this->db->where_not_in('p.id', [$product_id]);
        $this->db->limit(20);
        $query = $this->db->get();
        return $query->result();
    }

    public function products_by_filters($limit, $start, $category_id, $subcategory_id, $sortBy=null, $colour_id=null, $size_id=null)
    {
        if($colour_id && !$size_id){
            $product_id_array = $this->product_id_by_colour($colour_id); 
            if($product_id_array){
                $this->db->where_in('id', $product_id_array);
            }else{
                $this->db->where_in('id', [0]);
            }               
        }   
        
        if($size_id && !$colour_id){
            $product_id_array = $this->product_id_by_size($size_id); 
            if($product_id_array){
                $this->db->where_in('id', $product_id_array);
            }else{
                $this->db->where_in('id', [0]);
            }               
        }   

        if($colour_id && $size_id){
            $product_id_array = $this->product_id_by_colourandsize($colour_id, $size_id);             
            if($product_id_array){
                $this->db->where_in('id', $product_id_array);
            }else{
                $this->db->where_in('id', [0]);
            } 
        }

        if($sortBy == 'low-to-high'){
            $this->db->order_by('sale_price', 'ASC');
        }

        if($sortBy == 'high-to-low'){
            $this->db->order_by('sale_price', 'DESC');
        }

        if($sortBy == 'new'){
            $this->db->order_by('id', 'DESC');
        }

        if($subcategory_id){
            $this->db->where('subcategory_id', $subcategory_id);
        }

        $this->db->where('category_id', $category_id);


        if ($limit && $start) {
            $this->db->limit($start, $limit);
        }
        if(!$limit && $start){
            $this->db->limit($start);
        }
        $query = $this->db->get('products');
        return $query->result();
    }
    
    public function subcategories($category_id)
    {
        $this->db->select('id, name, slug');
        $this->db->where('category_id', $category_id);
        $this->db->where('is_active', 1);
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get('subcategories');
        return $query->result();
    }

    public function colours()
    {
        $this->db->select('id, name, slug');       
        $this->db->where('is_active', 1);
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get('colours');
        return $query->result();
    }

    public function sizes()
    {
        $this->db->select('id, name, slug');       
        $this->db->where('is_active', 1);
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get('sizes');
        return $query->result();
    }

    public function category_by_slug($slug)
    {
        $this->db->select('id');
        $this->db->where('is_active', 1);
        $this->db->where('slug', $slug);
        $query = $this->db->get('categories');
        return $query->row('id');
    }

    public function subcategory_by_slug($slug)
    {
        $this->db->select('id');
        $this->db->where('is_active', 1);
        $this->db->where('slug', $slug);
        $query = $this->db->get('subcategories');
        return $query->row('id');
    }

    public function product_id_by_colour($colour_id)
    {
        $colour_array = explode(',', $colour_id);        
        $this->db->select('product_id');
        $this->db->from('attributes');
        $this->db->where_in('colour_id', $colour_array);
        $this->db->group_by('product_id');
        $query = $this->db->get();
        $array = [];
        foreach($query->result() as $result)
        {
            array_push($array, $result->product_id);
        }
        return $array;
    }

    public function product_id_by_size($size_id)
    {
        $size_array = explode(',', $size_id);        
        $this->db->select('product_id');
        $this->db->from('attributes');
        $this->db->where_in('size_id', $size_array);
        $this->db->group_by('product_id');
        $query = $this->db->get();
        $array = [];
        foreach($query->result() as $result)
        {
            array_push($array, $result->product_id);
        }
        return $array;
    }

    public function product_id_by_colourandsize($colour_id, $size_id)
    {
        $colour_array = explode(',', $colour_id);  
        $size_array = explode(',', $size_id);        
        $this->db->select('product_id');
        $this->db->from('attributes');
        $this->db->where_in('colour_id', $colour_array);
        $this->db->where_in('size_id', $size_array);        
        $this->db->group_by('product_id');
        $query = $this->db->get();
        $array = [];
        foreach($query->result() as $result)
        {
            array_push($array, $result->product_id);
        }
        return $array;
    }
    
    public function products_by_category($category_id)
    {
        $this->db->where('category_id', $category_id);
        $query = $this->db->get('products');
        return $query->result();
    }

    public function subcategoryslug_by_subcategoryid($id)
    {
        $this->db->select('slug');
        $this->db->where('is_active', 1);
        $this->db->where('id', $id);
        $query = $this->db->get('subcategories');
        return $query->row('slug');
    }

    public function product_by_slug($slug)
    {
        $this->db->where('is_active', 1);
        $this->db->where('slug', $slug);
        $query = $this->db->get('products');
        return $query->row();
    }

    public function colours_by_product($product_id)
    {
        $this->db->select('c.id, c.name');
        $this->db->from('colours c');
        $this->db->join('attributes a', 'a.colour_id = c.id');
        $this->db->where('c.is_active', 1);
        $this->db->where('a.product_id', $product_id);
        $this->db->where('a.qty >', 0);
        $query = $this->db->get();
        return $query->result();
    }

    public function sizes_by_product($product_id)
    {
        $this->db->select('s.id, s.name');
        $this->db->from('sizes s');
        $this->db->join('attributes a', 'a.colour_id = s.id');
        $this->db->where('s.is_active', 1);
        $this->db->where('a.product_id', $product_id);
        $this->db->where('a.qty >', 0);
        $query = $this->db->get();
        return $query->result();
    }
}
