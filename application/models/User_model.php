<?php
class User_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'users';
    }

    public function sign_up($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function exist_phone($phone)
    {
        $this->db->where('phone', $phone);
        $this->db->where('role_id', 2);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function update_otp($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return true;
    }

    public function user_details($user_id)
    {
        $this->db->where('id', $user_id);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function exist_wishlist($product_id, $user_id)
    {
        $this->db->where('product_id', $product_id);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('wishlists');
        return $query->num_rows();
    }

    public function add_to_wishlist($data)
    {
        $this->db->insert('wishlists', $data);
        return $this->db->insert_id();
    }

    public function remove_from_wishlist($product_id, $user_id)
    {
        $this->db->where('product_id', $product_id);
        $this->db->where('user_id', $user_id);
        $this->db->delete('wishlists');
        return true;
    }

    public function delete_from_wishlist($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('wishlists');
        return true;
    }

    public function delete_from_cart($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('carts');
        return true;
    }

    public function wishlists($user_id)
    {
        $this->db->select('w.id, c.slug as category, s.slug as subcategory, p.slug, p.name, p.image_1, p.sale_price');
        $this->db->from('wishlists w');
        $this->db->join('products p', 'p.id = w.product_id');
        $this->db->join('categories c', 'c.id = p.category_id');
        $this->db->join('subcategories s', 's.id = p.subcategory_id');
        $this->db->where('w.user_id', $user_id);
        $this->db->order_by('w.id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function wishlist_count($user_id)
    {
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('wishlists');
        return $query->num_rows();
    }

    public function cart_count($user_id)
    {
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('carts');
        return $query->num_rows();
    }

    public function add_to_cart($data)
    {
        $this->db->insert('carts', $data);
        return $this->db->insert_id();
    }

    public function exist_cart($user_id, $product_id, $colour_id, $size_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('product_id', $product_id);
        $this->db->where('colour_id', $colour_id);
        $this->db->where('size_id', $size_id);
        $query = $this->db->get('carts');
        return $query->row();
    }

    public function update_cart_qty($id, $current_qty, $qty)
    {
        $data = ['qty' => $current_qty + $qty];
        $this->db->where('id', $id);
        $this->db->update('carts', $data);
        return true;
    }

    public function cart_amount($user_id)
    {
        $this->db->select('sum(p.sale_price * c.qty) as total');
        $this->db->from('carts c');
        $this->db->join('products p', 'p.id = c.product_id');
        $this->db->where('c.user_id', $user_id);
        $query = $this->db->get();
        return $query->row('total');
    }

    public function cart_items($user_id)
    {
        $this->db->select('c.id, ca.slug as category, su.slug as subcategory, p.slug, p.name, p.image_1, p.sale_price, si.name as size, co.name as colour, c.qty');
        $this->db->from('carts c');
        $this->db->join('products p', 'p.id = c.product_id');
        $this->db->join('categories ca', 'ca.id = p.category_id');
        $this->db->join('subcategories su', 'su.id = p.subcategory_id');
        $this->db->join('colours co', 'co.id = c.colour_id');
        $this->db->join('sizes si', 'si.id = c.colour_id');
        $this->db->where('user_id', $user_id);
        $this->db->order_by('c.id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }
}
